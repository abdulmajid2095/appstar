package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;


public class Petugas_Verifikasi extends Fragment implements OnClickListener, OnRefreshListener, OnItemClickListener{

	String[] judul;
	String[] gambar;
	String[] deskripsi;
	String[] id_masalah;
	String[] lokasi;
	String[] kategori;
	String[] cek;
	
	ListView hasil;
	private SwipeRefreshLayout swipeRefreshLayout;
	private static String TAG = Petugas_Verifikasi.class.getSimpleName();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_petugas__verifikasi, container, false);
         
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.petugas_swipe_refresh_layout1);
		swipeRefreshLayout.setOnRefreshListener(this);
		
        hasil = (ListView) rootView.findViewById(R.id.petugas_hasil1);
        hasil.setOnItemClickListener(this);
        ambildata();
        return rootView;
    }
    
    public void ambildata() {
    	swipeRefreshLayout.setRefreshing(true);
    	Dataku data = Dataku.findById(Dataku.class, 1L);
    	String id_petugas = data.id_kategori_petugas;
    	
    	//String json_alamat = "http://192.168.43.8/appstar/api/getAllPostBelumDitanganiForPetugas?id_petugas="+id_petugas;
		String json_alamat = "http://appstar.esy.es/api/getAllPostBelumDitanganiForPetugas?id_petugas="+id_petugas;
        
		JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                            
                        	judul = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	deskripsi = new String[jumlah_data];
                        	id_masalah = new String[jumlah_data];
                        	lokasi = new String[jumlah_data];
                        	kategori = new String[jumlah_data];
                        	cek = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                judul[i] = hasil.getString("title");
                                gambar[i] = hasil.getString("image");
                                deskripsi[i] = hasil.getString("caption");
                                id_masalah[i] = hasil.getString("id_post");
                                lokasi[i] = hasil.getString("location_name");
                                kategori[i] = hasil.getString("nama_kategori");
                                cek[i] = hasil.getString("id_petugas");
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                          
                           swipeRefreshLayout.setRefreshing(false);
                        }
                                              
                        Adapter_Petugas_Hasil adapter = new Adapter_Petugas_Hasil(getActivity(), judul, gambar, deskripsi, lokasi, kategori, cek);
                        hasil.setAdapter(adapter);
                      
                       
                        
                        adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                      
                        swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
        
    }

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		ambildata();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemClick(AdapterView arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		
		/*Toast.makeText(getActivity(),
                "Error: "+judul[posisi],
                Toast.LENGTH_LONG).show();*/
		
		String id_post = id_masalah[posisi];
		Dataku data = Dataku.findById(Dataku.class, 1L);
		//String json_alamat = "http://192.168.43.8/appstar/api/tanggapiPost?id_post="+id_post;
		String json_alamat = "http://appstar.esy.es/api/tanggapiPost?id_post="+id_post+"&id_petugas="+data.id_userku;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                 
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
        
        ambildata();
	}
  
}