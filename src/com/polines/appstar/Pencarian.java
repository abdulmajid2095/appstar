package com.polines.appstar;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

public class Pencarian extends FragmentActivity implements OnClickListener{

	Button menu1,menu2,menu3,menu4,menu5;
	ImageButton maps;
	
	String Tab1="Tab1",Tab2="Tab2";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_pencarian);
		
		menu1=(Button) findViewById(R.id.menu12);
		menu1.setOnClickListener(this);
		menu2=(Button) findViewById(R.id.menu22);
		menu2.setOnClickListener(this);
		menu3=(Button) findViewById(R.id.menu32);
		menu3.setOnClickListener(this);
		menu4=(Button) findViewById(R.id.menu42);
		menu4.setOnClickListener(this);
		menu5=(Button) findViewById(R.id.menu52);
		menu5.setOnClickListener(this);
		
		maps = (ImageButton) findViewById(R.id.ke_maps);
		maps.setOnClickListener(this);
		

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fm = getSupportFragmentManager();
        Adapter_Pencarian_Tab pagerAdapter = new Adapter_Pencarian_Tab(fm);
        pager.setAdapter(pagerAdapter);
		
	}

	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
			case R.id.menu12:
				Intent beranda = new Intent(Pencarian.this, Beranda.class);
				startActivity(beranda);
			break;
			
			case R.id.menu22:
				Intent pencarian = new Intent(Pencarian.this, Pencarian.class);
				startActivity(pencarian);
			break;
			
			case R.id.menu32:
				Intent post = new Intent(Pencarian.this, Post.class);
				startActivity(post);
			break;
			
			case R.id.menu42:
				Intent notifikasi = new Intent(Pencarian.this, Notifikasi.class);
				startActivity(notifikasi);
			break;
			
			case R.id.menu52:
				Intent profile = new Intent(Pencarian.this, Profile.class);
				startActivity(profile);
			break;
			
			case R.id.ke_maps:
				Intent kemaps = new Intent(Pencarian.this, Maps.class);
				startActivity(kemaps);
			break;
		}
		
	}

}
