package com.polines.appstar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_Petugas_Tab extends FragmentPagerAdapter {

	public Adapter_Petugas_Tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            return new Petugas_Verifikasi();
        case 1:
        	return new Petugas_Unverifikasi();
        case 2:
        	return new Petugas_Pencarian();
        }
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
       
    	switch (position) {
        case 0:
        	return "Belum Ditangani";
        case 1:
        	return "Sudah Ditangani";
        case 2:
        	return "Pencarian";
        }
 
        return null;
    }

}