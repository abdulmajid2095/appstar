package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Notifikasi extends Activity implements OnClickListener, OnRefreshListener, OnItemClickListener{

	private static String TAG = Notifikasi.class.getSimpleName();
	
	ListView list_notif;
	
	Button menu1,menu2,menu3,menu4,menu5;
	
	//notif
	String notif,notif2;
	int notif_tampil,jumlah_notif=0,jumlah_notif2=0;
	ImageView notif_round;
	TextView notif_jumlah,coba_notif;
	
	String get_id[];
	String get_kategori[];
	String get_gambar[];
	String get_name[];
	
	Button loadmore;
	
	private SwipeRefreshLayout swipeRefreshLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_notifikasi);
		
		menu1=(Button) findViewById(R.id.menu14);
		menu1.setOnClickListener(this);
		menu2=(Button) findViewById(R.id.menu24);
		menu2.setOnClickListener(this);
		menu3=(Button) findViewById(R.id.menu34);
		menu3.setOnClickListener(this);
		menu4=(Button) findViewById(R.id.menu44);
		menu4.setOnClickListener(this);
		menu5=(Button) findViewById(R.id.menu54);
		menu5.setOnClickListener(this);
		
		notif_round = (ImageView) findViewById(R.id.notif_round);
		notif_jumlah = (TextView) findViewById(R.id.notif_teks_jumlah);
		coba_notif = (TextView) findViewById(R.id.coba_notif);
		
		Intent baru = getIntent();
		notif = baru.getStringExtra("notif");
		notif2 = baru.getStringExtra("notif2");
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.notifikasi_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		
		if(notif==null&&notif2==null)
		{
			
			jumlah_notif=10;
		}
		else
		{
			jumlah_notif = Integer.parseInt(notif);
			jumlah_notif2 = Integer.parseInt(notif2);
			set_notif();
		}
		
		list_notif = (ListView) findViewById(R.id.list_notif);
		list_notif.setOnItemClickListener(this);
		View myFooterView = getLayoutInflater().inflate(R.layout.footer_beranda,list_notif,false);
		list_notif.addFooterView(myFooterView,null,false);
		
		loadmore = (Button) myFooterView.findViewById(R.id.beranda_loadmore);
		loadmore.setOnClickListener(this);
		
		ambilnotif();
		
	}

	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
			case R.id.menu14:
				Intent beranda = new Intent(Notifikasi.this, Beranda.class);
				startActivity(beranda);
			break;
			
			case R.id.menu24:
				Intent pencarian = new Intent(Notifikasi.this, Pencarian.class);
				startActivity(pencarian);
			break;
			
			case R.id.menu34:
				Intent post = new Intent(Notifikasi.this, Post.class);
				startActivity(post);
			break;
			
			case R.id.menu44:
				Intent notifikasi = new Intent(Notifikasi.this, Notifikasi.class);
				startActivity(notifikasi);
			break;
			
			case R.id.menu54:
				Intent profile = new Intent(Notifikasi.this, Profile.class);
				startActivity(profile);
			break;
			
			case R.id.beranda_loadmore:
				jumlah_notif = jumlah_notif+10;
				ambilnotif();
				
			break;
		}
		
	}
	
	
	public void set_notif()
	{
		Dataku data2 = Dataku.findById(Dataku.class, 1L);
		String id_user = data2.id_userku;
		
		//String url = "http://192.168.43.8/appstar/api/update_notif?id_user="+id_user+"&notif="+jumlah_notif2;
		String url = "http://appstar.esy.es/api/update_notif?id_user="+id_user+"&notif="+jumlah_notif2;
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                 
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
	}
	
	
	
	private void ambilnotif() {
    	swipeRefreshLayout.setRefreshing(true);
    	Dataku data2 = Dataku.findById(Dataku.class, 1L);
		String id_user = data2.id_userku;
		//String json_alamat = "http://http://192.168.43.8/appstar/api/tampil_notif?id_user="+id_user+"&limit="+jumlah_notif;
		String json_alamat = "http://appstar.esy.es/api/tampil_notif?id_user="+id_user+"&limit="+jumlah_notif;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                        	
                        	get_id = new String[jumlah_data];
                        	get_kategori = new String[jumlah_data];
                        	get_gambar = new String[jumlah_data];
                        	get_name = new String[jumlah_data];
                        	
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	get_id[i] = hasil.getString("id");
                                	get_kategori[i] = hasil.getString("id_kategori_notif");
                                	get_gambar[i] = hasil.getString("image");
                                	get_name[i] = hasil.getString("fullname");
                                    
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                           
                           swipeRefreshLayout.setRefreshing(false);
                        }
                        
                        Adapter_notifikasi adapter = new Adapter_notifikasi(Notifikasi.this, get_kategori,get_gambar,get_name);
                		list_notif.setAdapter(adapter);
                      
                        
                        adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                   
                        swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(req);
        
    }

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		ambilnotif();
	}

	@Override
	public void onItemClick(AdapterView arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		
		String kategori1 = "1";
		String kategori2 = "2";
		String kategori3 = "3";
		String kategori4 = "4";
		
		if(get_kategori[posisi].equals(kategori1))
		{
			Intent intent = new Intent(Notifikasi.this,Profile.class);
			intent.putExtra("datakirim", ""+get_id[posisi]);
			startActivity(intent);
		}
		if(get_kategori[posisi].equals(kategori2))
		{
			Intent intent = new Intent(Notifikasi.this,Masalah.class);
			intent.putExtra("datakirim", ""+get_id[posisi]);
			startActivity(intent);
		}
		if(get_kategori[posisi].equals(kategori3))
		{
			Intent intent = new Intent(Notifikasi.this,Masalah.class);
			intent.putExtra("datakirim", ""+get_id[posisi]);
			startActivity(intent);
		}
		if(get_kategori[posisi].equals(kategori4))
		{
			Intent intent = new Intent(Notifikasi.this,Masalah.class);
			intent.putExtra("datakirim", ""+get_id[posisi]);
			startActivity(intent);
		}
		
	}
	
}
