package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
 
public class Pencarian_Pengguna extends Fragment implements OnClickListener, OnItemClickListener, OnRefreshListener{
 
	EditText pencarian;
	ImageButton tombol_cari;
	String cari="";
	ListView hasil_pencarian_user;
	String[] user;
	String[] gambar;
	String[] fullname;
	String [] id_user;
	String json_alamat;
	int jumlah_data,i;
	JSONObject hasil;
	Activity context;
	private SwipeRefreshLayout swipeRefreshLayout;
	
	private ProgressDialog pDialog;
	private static String TAG = Pencarian_Masalah.class.getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

    	context=getActivity();
    	pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Mencari...");
        pDialog.setCancelable(false);
            	
        View rootView = inflater.inflate(R.layout.activity_pencarian__pengguna, container, false);
        
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.pencarian_user_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);

        hasil_pencarian_user = (ListView) rootView.findViewById(R.id.hasil_pencarian_pengguna);
        hasil_pencarian_user.setOnItemClickListener(this);
        
        ambildataawal();
        return rootView;
    }
	@Override
	public void onClick(View pilihan) {
		switch (pilihan.getId()) {
		case R.id.tbl_cari:
			cari = pencarian.getText().toString();
			ambildata();
			break;

		default:
			break;
		}
		
	}
	

	
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    private void ambildataawal() {
    	//json_alamat = "http://192.168.43.8/appstar/api/cari_user?search="+cari;
		json_alamat = "http://appstar.esy.es/api/cari_user?search="+cari;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	jumlah_data = response.length();
                            
                            
                        	user = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	fullname = new String[jumlah_data];
                        	id_user = new String[jumlah_data];
                        	
                        	for (i = 0; i < response.length(); i++) {
                        	
                                hasil = (JSONObject) response
                                        .get(i);
                                
                                	user[i] = hasil.getString("username");
                                    gambar[i] = hasil.getString("avatar");
                                    fullname[i] = hasil.getString("fullname");
                                    id_user[i] = hasil.getString("id_user");
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                        	swipeRefreshLayout.setRefreshing(false);
                        }
                        
                        Adapter_Pencarian_User adapter = new Adapter_Pencarian_User(getActivity(), user, gambar, fullname);
                		hasil_pencarian_user.setAdapter(adapter);
                		adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
                    	
                    	swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
        
    }
    
    private void ambildata() {
    	//json_alamat = "http://192.168.43.8/appstar/api/cari_user?search="+cari;
		json_alamat = "http://appstar.esy.es/api/cari_user?search="+cari;
    	swipeRefreshLayout.setRefreshing(true);
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	jumlah_data = response.length();
                            
                            
                        	user = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	fullname = new String[jumlah_data];
                        	id_user = new String[jumlah_data];
                        	
                        	for (i = 0; i < response.length(); i++) {
                        	
                                hasil = (JSONObject) response
                                        .get(i);
                                
                                	user[i] = hasil.getString("username");
                                    gambar[i] = hasil.getString("avatar");
                                    fullname[i] = hasil.getString("fullname");
                                    id_user[i] = hasil.getString("id_user");
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                        	swipeRefreshLayout.setRefreshing(false);
                        	peringatan2();
                           
                        }
                        
                        Adapter_Pencarian_User adapter = new Adapter_Pencarian_User(getActivity(), user, gambar, fullname);
                		hasil_pencarian_user.setAdapter(adapter);
                        hidepDialog();
                        
                        adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
                    	swipeRefreshLayout.setRefreshing(false);
                    	peringatan();
                    	
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
        showpDialog();
        
    }
    
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {
            	pencarian = (EditText) getActivity().findViewById(R.id.teks_cari);
                tombol_cari = (ImageButton) getActivity().findViewById(R.id.tbl_cari);
                tombol_cari.setOnClickListener(this);
            }
        }
    }
    
    
    public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(getActivity());
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Pencarian gagal, Cek Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    
    public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(getActivity());
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Hasil Tidak Ditemukan");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
	@Override
	public void onItemClick(AdapterView arg0, View arg1, int posisi, long arg3) {

		Intent intent = new Intent(context,Profile.class);
		intent.putExtra("datakirim", ""+id_user[posisi]);
		startActivity(intent);
		
		
	}
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		ambildata();
	}

    
}