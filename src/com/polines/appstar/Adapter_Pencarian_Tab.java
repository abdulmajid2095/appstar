package com.polines.appstar;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Adapter_Pencarian_Tab extends FragmentPagerAdapter {

	public Adapter_Pencarian_Tab(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int index) {
		switch (index) {
        case 0:
            // Top Rated fragment activity
            return new Pencarian_Masalah();
        case 1:
            // Games fragment activity
        	return new Pencarian_Pengguna();
        }
 
        return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 2;
	}
	
	@Override
    public CharSequence getPageTitle(int position) {
        //return "Page #" + ( position + 1 );
    	switch (position) {
        case 0:
            // Top Rated fragment activity
        	return "Masalah";
        case 1:
            // Games fragment activity
        	return "Pengguna";
        }
 
        return null;
    }

}
