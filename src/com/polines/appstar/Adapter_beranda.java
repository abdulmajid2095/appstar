package com.polines.appstar;


import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_beranda extends ArrayAdapter<String> implements OnClickListener {

	String[] judul={};
	String[] lokasi={};
	String[] waktu={};
	String[] gambar={};
	String[] pengapload={};
	String[] kategori={};
	String[] validasi={};
	String[] nama_pengaploadku={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_beranda(Context context, String[]judul, String[]lokasi, String[]waktu, String[]gambar, String[]pengapload, String[]kategori, String[]validasi, String[]nama_pengaploadku) {
		super(context, R.layout.model_beranda,judul);
		
		this.c = context;
		this.judul = judul;
		this.lokasi = lokasi;
		this.waktu = waktu;
		this.gambar = gambar;
		this.pengapload = pengapload;
		this.kategori = kategori;
		this.validasi = validasi;
		this.nama_pengaploadku = nama_pengaploadku;
	}
	
	public class ViewHolder
	{
		TextView judulku ;
		TextView lokasiku;
		TextView waktuku;
		ImageView gambarku;
		ImageView pengaploadku;
		TextView kategoriku;
		LinearLayout layout_validasiku;
		TextView teks_validasiku;
		TextView nama_pengapload;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_beranda,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		holder.judulku = (TextView) convertView.findViewById(R.id.beranda_judul);
		holder.judulku.setText(judul[position]);
		
		holder.lokasiku = (TextView) convertView.findViewById(R.id.beranda_lokasi);
		holder.lokasiku.setText(lokasi[position]);
		
		holder.waktuku = (TextView) convertView.findViewById(R.id.beranda_waktu);
		holder.waktuku.setText(waktu[position]);
		
		holder.kategoriku = (TextView) convertView.findViewById(R.id.beranda_kategori);
		if(kategori[position].equals("1"))
		{
			holder.kategoriku.setText("Lalu Lintas");
		}
		if(kategori[position].equals("2"))
		{
			holder.kategoriku.setText("Parkir");
		}
		if(kategori[position].equals("3"))
		{
			holder.kategoriku.setText("Transportasi");
		}
		if(kategori[position].equals("4"))
		{
			holder.kategoriku.setText("Lingkungan");
		}
		if(kategori[position].equals("5"))
		{
			holder.kategoriku.setText("Banjir");
		}
		if(kategori[position].equals("6"))
		{
			holder.kategoriku.setText("Ruang Terbuka Hijau");
		}
		if(kategori[position].equals("7"))
		{
			holder.kategoriku.setText("Perijinan");
		}
		if(kategori[position].equals("8"))
		{
			holder.kategoriku.setText("Lampu PJU");
		}
		if(kategori[position].equals("9"))
		{
			holder.kategoriku.setText("Kebakaran");
		}
		if(kategori[position].equals("10"))
		{
			holder.kategoriku.setText("Pelayanan Publik");
		}
		if(kategori[position].equals("11"))
		{
			holder.kategoriku.setText("Akses Wifi");
		}
		if(kategori[position].equals("12"))
		{
			holder.kategoriku.setText("CFD");
		}
		if(kategori[position].equals("13"))
		{
			holder.kategoriku.setText("Kebersihan");
		}
		if(kategori[position].equals("14"))
		{
			holder.kategoriku.setText("Pungli");
		}
		if(kategori[position].equals("15"))
		{
			holder.kategoriku.setText("Infrastruktur");
		}
		if(kategori[position].equals("16"))
		{
			holder.kategoriku.setText("Keamanan");
		}
		
		holder.nama_pengapload =(TextView) convertView.findViewById(R.id.beranda_username);
		holder.nama_pengapload.setText(""+nama_pengaploadku[position]);
		
		holder.gambarku = (ImageView) convertView.findViewById(R.id.beranda_gambar);
		Picasso.with(getContext())
		.load("http://appstar.esy.es/uploads/post/"+gambar[position])
		//.load("http://192.168.43.8/appstar/uploads/post/"+gambar[position])
		.resize(600, 400)
        .centerCrop()
        .into(holder.gambarku);
		
		holder.pengaploadku = (ImageView) convertView.findViewById(R.id.beranda_pengapload);
		Picasso.with(getContext())
		.load("http://appstar.esy.es/uploads/avatar/"+pengapload[position])
		//.load("http://192.168.43.8/appstar/uploads/avatar/"+pengapload[position])
		.resize(300, 300)
        .centerCrop()
        .transform(new CircleTransform())
        .into(holder.pengaploadku);
		
		
		
		String nu = "null";
		holder.layout_validasiku = (LinearLayout) convertView.findViewById(R.id.beranda_layout_validasi);
		holder.teks_validasiku = (TextView) convertView.findViewById(R.id.beranda_teks_validasi);
		if(validasi[position].equals(nu) )
		{
			holder.teks_validasiku.setText("Belum Ditanggapi");
			holder.layout_validasiku.setBackgroundColor(Color.rgb(210, 51, 51));
		}
		else
		{
			holder.teks_validasiku.setText("Sudah Ditanggapi");
			holder.layout_validasiku.setBackgroundColor(Color.rgb(72, 207, 72));
		}
		return convertView;
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	

}
