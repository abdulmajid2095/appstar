package com.polines.appstar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class Masalah extends Activity implements OnClickListener, OnItemClickListener, OnRefreshListener{
	
	private SwipeRefreshLayout swipeRefreshLayout;
	ListView komentar;
	//komentar
	String[] id_user;
	String[] user;
	String[] gambar;
	String[] komentarku;
	String[] foto;
	String[] id_komentar;
	
	String[] id_masalah;
	private static String TAG = Masalah.class.getSimpleName();
	
	//Ambil Deskripsi
	ImageButton pengapload,tbl_komentar,tbl_support,tbl_share,tbl_opsi;
	TextView teks_validasi,nama_pengapload,judul_masalah,deskripsi_masalah,jumlah_komentar,waktu,kategori;
	ImageView gambar_masalah;
	LinearLayout layout_validasi;
	//Menu
	Button menu1,menu2,menu3,menu4,menu5,lokasi_masalah;
	
	String get_id_post,get_latitude,get_longitude,get_waktu,get_kategori,get_validasi,id_kirim,get_id_pengapload,get_nama_pengapload,get_judul,get_lokasi,get_deskripsi,get_gambar_masalah,get_gambar_pengapload;
	int get_jumlah_komentar,posisiku;
	
	//Post Komentar
	String komen_kategori,komen_id_post,komen_id_user,komen_komentar;
	
	private ProgressDialog pDialog;
	Dialog dialog_komen,dialog_support;
	
	
	ImageButton pilih_gambar;
	String pilihan,lokasi_gambar,nama_gambar;
	String pilihan1="kamera";
	String pilihan2="galeri";
	final int CAMERA_REQUEST = 12345;
	int TAKE_PHOTO_CODE = 0;
    public static int count = 0;
	private static final int SELECTED_PICTURE=1;
	Bitmap gambarku;
	int gambar_dipilih=0;
	String urlku;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_masalah);
		
		Intent baru = getIntent();
		id_kirim = baru.getStringExtra("datakirim");
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("");
        pDialog.setCancelable(false);
        
		menu1=(Button) findViewById(R.id.menu1m);
		menu1.setOnClickListener(this);
		menu2=(Button) findViewById(R.id.menu2m);
		menu2.setOnClickListener(this);
		menu3=(Button) findViewById(R.id.menu3m);
		menu3.setOnClickListener(this);
		menu4=(Button) findViewById(R.id.menu4m);
		menu4.setOnClickListener(this);
		menu5=(Button) findViewById(R.id.menu5m);
		menu5.setOnClickListener(this);
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.komentar_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		
		komentar = (ListView) findViewById(R.id.masalah_komentar);
		komentar.setOnItemClickListener(this);
		View myHeaderView = getLayoutInflater().inflate(R.layout.model_masalah,komentar,false);
		komentar.addHeaderView(myHeaderView,null,false);
		komentar.setAdapter(null);
		
		pengapload = (ImageButton) findViewById(R.id.masalah_user_upload_gambar);
		pengapload.setOnClickListener(this);
		tbl_komentar = (ImageButton) findViewById(R.id.masalah_tbl_komentar);
		tbl_komentar.setOnClickListener(this);
		tbl_support = (ImageButton) findViewById(R.id.masalah_tbl_support);
		tbl_support.setOnClickListener(this);
		tbl_share = (ImageButton) findViewById(R.id.masalah_tbl_share);
		tbl_share.setOnClickListener(this);
		nama_pengapload = (TextView) findViewById(R.id.masalah_user_upload);
		judul_masalah = (TextView) findViewById(R.id.masalah_judul);
		lokasi_masalah = (Button) findViewById(R.id.masalah_lokasi);
		lokasi_masalah.setOnClickListener(this);
		deskripsi_masalah = (TextView) findViewById(R.id.masalah_deskripsi);
		jumlah_komentar = (TextView) findViewById(R.id.masalah_semua_komentar);
		gambar_masalah = (ImageView) findViewById(R.id.masalah_gambar);
		tbl_komentar = (ImageButton) findViewById(R.id.masalah_tbl_komentar);
		tbl_komentar.setOnClickListener(this);
		tbl_support = (ImageButton) findViewById(R.id.masalah_tbl_support);
		tbl_support.setOnClickListener(this);
		tbl_share = (ImageButton) findViewById(R.id.masalah_tbl_share);
		tbl_share.setOnClickListener(this);
		tbl_opsi = (ImageButton) findViewById(R.id.masalah_tbl_opsi);
		tbl_opsi.setOnClickListener(this);
		waktu = (TextView) findViewById(R.id.masalah_waktu);
		layout_validasi = (LinearLayout) findViewById(R.id.masalah_layout_validasi);
		teks_validasi = (TextView) findViewById(R.id.masalah_teks_validasi);
		kategori = (TextView) findViewById(R.id.masalah_kategori);
		
		ambil_deskripsi();
		ambil_komentar();
	}

	
	@Override
	public void onClick(View pilihan) {
		// TODO Auto-generated method stub
		switch (pilihan.getId()) {
			case R.id.menu1m:
				Intent beranda = new Intent(Masalah.this, Beranda.class);
				startActivity(beranda);
			break;
			
			case R.id.menu2m:
				Intent pencarian = new Intent(Masalah.this, Pencarian.class);
				startActivity(pencarian);
			break;
			
			case R.id.menu3m:
				Intent post = new Intent(Masalah.this, Post.class);
				startActivity(post);
			break;
			
			case R.id.menu4m:
				Intent notifikasi = new Intent(Masalah.this, Notifikasi.class);
				startActivity(notifikasi);
			break;
			
			case R.id.menu5m:
				Intent profile = new Intent(Masalah.this, Profile.class);
				startActivity(profile);
			break;
			
			case R.id.masalah_tbl_komentar:
				dialog_komentar();
			break;
			
			case R.id.masalah_tbl_support:
				dialog_support();
			break;
			
			case R.id.masalah_tbl_share:
				/*String text = "Look at my awesome picture";
				Uri pictureUri =  getLocalBitmapUri(gambar_masalah);
				Intent shareIntent = new Intent();
				shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
				shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
				shareIntent.setType("image/*");
				shareIntent.setType("text/plain"); 
				shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				startActivity(Intent.createChooser(shareIntent, "Share Masalah"));*/
				
				/*Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType("text/plain"); 
				shareIntent.putExtra(Intent.EXTRA_TEXT, get_judul+"\n\n"+get_deskripsi+"\n\n"+"http://appstar.esy.es/app/problem/"+get_id_post);
				startActivity(Intent.createChooser(shareIntent, "Share link using"));*/
				notif_share();
				hidepDialog();
				Uri pictureUri =  getLocalBitmapUri(gambar_masalah);
				Intent shareIntent = new Intent();
				shareIntent.setAction(Intent.ACTION_SEND);
				shareIntent.putExtra(Intent.EXTRA_TEXT, get_judul+"\n\n"+get_deskripsi+"\n\n"+"http://appstar.esy.es/app/problem/"+get_id_post);
				shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri);
				shareIntent.setType("image/*");
				shareIntent.setType("text/plain");
				shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				startActivity(Intent.createChooser(shareIntent, "Share Masalah"));
			break;
			
			case R.id.masalah_user_upload_gambar:
				Intent intent = new Intent(Masalah.this,Profile.class);
				intent.putExtra("datakirim", ""+get_id_pengapload);
				startActivity(intent);
			break;
			case R.id.masalah_lokasi:
				Intent intentku = new Intent(Masalah.this,Maps.class);
				intentku.putExtra("latitude", ""+get_latitude);
				intentku.putExtra("longitude", ""+get_longitude);
				startActivity(intentku);
			break;
			
			case R.id.masalah_tbl_opsi:
				dialog_opsi();
			break;
		}
		
	}
	
	
	public void ambil_deskripsi() {
		//String url = "http://192.168.43.8/appstar/api/detail_masalah?id_post="+id_kirim;
    	String url = "http://appstar.esy.es/api/detail_masalah?id_post="+id_kirim;
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
	                        try {
	                            JSONObject respon = (JSONObject) response
	                                    .get(0);
	                           
	                           get_deskripsi = respon.getString("caption");
	                           get_gambar_masalah = respon.getString("image");
	                           get_id_pengapload = respon.getString("user_id_user");
	                           get_judul = respon.getString("title");
	                           get_lokasi = respon.getString("location_name");
	                           get_nama_pengapload = respon.getString("fullname");
	                           get_gambar_pengapload = respon.getString("avatar");
	                           get_validasi = respon.getString("id_petugas");
	                           get_waktu = respon.getString("create_date");
	                           get_kategori = respon.getString("nama_kategori");
	                           get_latitude = respon.getString("latitude");
	                           get_longitude = respon.getString("longitude");
	                           get_id_post = respon.getString("id_post");
	                           
	                           nama_pengapload.setText(""+get_nama_pengapload);
	                           judul_masalah.setText(""+get_judul);
	                           lokasi_masalah.setText(""+get_lokasi);
	                           deskripsi_masalah.setText(""+get_deskripsi);
	                           waktu.setText(""+get_waktu);
	                           kategori.setText(""+get_kategori);
	                           Picasso.with(getApplicationContext())
		                   		.load("http://appstar.esy.es/uploads/avatar/"+get_gambar_pengapload)
		                   		//.load("http://192.168.43.8/appstar/uploads/avatar/"+get_gambar_pengapload)
		                   		.resize(300, 300)
	                           .centerCrop()
	                           .transform(new CircleTransform())
	                           .into(pengapload);
	                           
	                           Picasso.with(getApplicationContext())
		                   		.load("http://appstar.esy.es/uploads/post/"+get_gambar_masalah)
		                   		//.load("http://192.168.43.8/appstar/uploads/post/"+get_gambar_masalah)
		                   		.resize(400, 600)
	                           .centerCrop()
	                           .into(gambar_masalah);
	                           
	                           String nu = "null";
		                   		if(get_validasi.equals(nu) )
		                   		{
		                   			teks_validasi.setText("Belum Ditanggapi");
		                   			layout_validasi.setBackgroundColor(Color.rgb(210, 51, 51));
		                   		}
		                   		else
		                   		{
		                   			teks_validasi.setText("Sudah Ditanggapi");
		                   			layout_validasi.setBackgroundColor(Color.rgb(72, 207, 72));
		                   		}
		                   		
		                   		Dataku data = Dataku.findById(Dataku.class, 1L);
		                   		if(data.id_userku.equals(get_id_pengapload))
		                   		{
		                   			tbl_opsi.setVisibility(View.VISIBLE);
		                   		}
		                   		else
		                   		{
		                   			tbl_opsi.setVisibility(View.INVISIBLE);
		                   		}
		                     
		                    } catch (JSONException e) {
		                        e.printStackTrace();
		                        Toast.makeText(getApplicationContext(),
	                                    "Error: " + e.getMessage(),
	                                    Toast.LENGTH_LONG).show();
		                    }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	error.printStackTrace();        
                    	Toast.makeText(getApplicationContext(),
                                "Error: " + error.getMessage(),
                                Toast.LENGTH_LONG).show();
                        
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }
	
	public void ambil_komentar() {
		swipeRefreshLayout.setRefreshing(true);
		//String url = "http://192.168.43.8/appstar/api/komentar_masalah?id_post="+id_kirim;
    	String url = "http://appstar.esy.es/api/komentar_masalah?id_post="+id_kirim;
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                        	jumlah_komentar.setText(""+jumlah_data+" Komentar");
                        	
                        	user = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	komentarku = new String[jumlah_data];
                        	id_user = new String[jumlah_data];
                        	foto = new String[jumlah_data];
                        	id_komentar = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	user[i] = hasil.getString("fullname");
                                    gambar[i] = hasil.getString("avatar");
                                    komentarku[i] = hasil.getString("isi_komentar");
                                    id_user[i] = hasil.getString("user_id_user");
                                    foto[i] = hasil.getString("image");
                                    id_komentar[i] = hasil.getString("id_komentar");
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                           
                        }
                        
                        Adapter_masalah adapter = new Adapter_masalah(getApplicationContext(), user, gambar, komentarku, foto);
                        komentar.setAdapter(adapter);
                        komentar.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, final int posisi, long arg3) {
								
								// TODO Auto-generated method stub
								/*Intent intent = new Intent(Masalah.this,Profile.class);
								intent.putExtra("datakirim", ""+id_user[posisi-1]);
								startActivity(intent);*/
								Dataku data = Dataku.findById(Dataku.class, 1L);
								if(id_user[posisi-1].equals(data.id_userku)|| get_id_pengapload.equals(data.id_userku))
								{
									// custom dialog
							        final Dialog dialog = new Dialog(Masalah.this);
							        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							        dialog.setContentView(R.layout.model_hapus_komentar);
							        
							        Button edit = (Button) dialog.findViewById(R.id.opsi_hapus_ya);
							        edit.setOnClickListener(new OnClickListener() {
										
										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub
											dialog.dismiss();
											String url = "http://appstar.esy.es/api/delete_komentar?id_komentar="+id_komentar[posisi-1];
									        JsonArrayRequest req = new JsonArrayRequest(url,
									                new Response.Listener<JSONArray>() {
									                    @Override
									                    public void onResponse(JSONArray response) {
									                    	
									                        Log.d(TAG, response.toString());
									                        
									                    }
									                }, new Response.ErrorListener() {
									                    @Override
									                    public void onErrorResponse(VolleyError error) {
									                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
									                 
									                    }
									                });
									        // Adding request to request queue
									        req.setRetryPolicy(new DefaultRetryPolicy(
													30000,
													DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
													DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
													));
									        RequestQueue requestQueue = Volley.newRequestQueue(Masalah.this);
									        requestQueue.add(req);
											ambil_komentar();
										}
									});
							        Button hapus = (Button) dialog.findViewById(R.id.opsi_hapus_tidak);
							        hapus.setOnClickListener(new OnClickListener() {
										
										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub
											dialog.dismiss();
										}
									});
							               
							        dialog.show();
								}
							}
						});
                        adapter.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                                         	
                    	swipeRefreshLayout.setRefreshing(false);
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }

	
	private void dialog_komentar() {

        // custom dialog
        dialog_komen = new Dialog(this);
        dialog_komen.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_komen.setContentView(R.layout.popup_komentar);
        final TextView isi_komen = (TextView) dialog_komen.findViewById(R.id.popup_komentar_isikomen);
        Button post_komen = (Button) dialog_komen.findViewById(R.id.popup_komentar_tbl);
        post_komen.setOnClickListener(new OnClickListener() {
	        public void onClick(View v) {
	        	
	        	Dataku data = Dataku.findById(Dataku.class, 1L);
	        	komen_kategori="1";
	        	komen_komentar = isi_komen.getText().toString();
	        	komen_komentar = komen_komentar.replace(' ', '+');
	            komen_id_post = id_kirim;
	            komen_id_user = data.id_userku;
	            
	            //urlku = "http://192.168.43.8/appstar/api/postHelp?kategori="+komen_kategori+"&id_post="+komen_id_post+"&id_user="+komen_id_user+"&komentar="+komen_komentar;
	        	urlku = "http://appstar.esy.es/api/postHelp?kategori="+komen_kategori+"&id_post="+komen_id_post+"&id_user="+komen_id_user+"&komentar="+komen_komentar;
	            
	            String cek = "";
	            if(komen_komentar.equals(cek))
	            {
	            	peringatan();
	            }
	            else
	            {
		            post_komentar();
	            }
	        }
	    });
        dialog_komen.show();

    }
	
	private void dialog_support() {

        // custom dialog
        dialog_support = new Dialog(this);
        dialog_support.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_support.setContentView(R.layout.popup_support);
        final TextView isi_komen = (TextView) dialog_support.findViewById(R.id.popup_support_isisupport);
        Button post_support = (Button) dialog_support.findViewById(R.id.popup_support_tbl);
        post_support.setOnClickListener(new OnClickListener() {
	        public void onClick(View v) {
	        	Long timestamp = System.currentTimeMillis()/1000;
				String timestampku = timestamp.toString();
				nama_gambar = id_kirim+timestampku;
				
	        	Dataku data = Dataku.findById(Dataku.class, 1L);
	        	komen_kategori="2";
	        	komen_komentar = isi_komen.getText().toString();
	        	komen_komentar = komen_komentar.replace(' ', '+');
	            komen_id_post = id_kirim;
	            komen_id_user = data.id_userku;
	            
	            
	            
	            String cek = "";
	            if(komen_komentar.equals(cek))
	            {
	            	peringatan();
	            }
	            else
	            {
	            	if(gambar_dipilih==1)
	            	{
	            		new UploadGambar( gambarku, nama_gambar).execute();
	            	}
	            	else
	            	{
	            		peringatan2();
	            	}
	            		
	            }
	            
	        }
	    });
        
        pilih_gambar = (ImageButton) dialog_support.findViewById(R.id.popup_support_gambar_pilih);
        pilih_gambar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog_pilihan();
			}
		});
        
        dialog_support.show();

    }
	
	public void post_komentar()
	{
		
        JsonArrayRequest req = new JsonArrayRequest(urlku,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        hidepDialog();
                      
                        Intent intent = new Intent(Masalah.this,Masalah.class);
	    	    		intent.putExtra("datakirim", ""+komen_id_post);
	    	    		startActivity(intent);	
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                    	hidepDialog(); 
                    	//dialog_komen.dismiss();
                    	}
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
	}
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
    public void dialog_pilihan()
	{
		AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Pilih Gambar");
		dialogkeluar.setNegativeButton ("Ambil Gambar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				pilih_kamera();
			}
		});
		dialogkeluar.setPositiveButton("Galeri", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				pilih_galeri();
			}
		});
		dialogkeluar.show();
	}
	
	
	public void pilih_kamera()
	{
		Intent kamera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(kamera,CAMERA_REQUEST);
	}
	
	public void pilih_galeri()
	{
		Intent galeri = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galeri, SELECTED_PICTURE);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK)
		{
			
			if(requestCode == CAMERA_REQUEST)
			{
				 	gambarku = (Bitmap) data.getExtras().get("data");
				 	pilih_gambar.setImageBitmap(gambarku);
					gambar_dipilih = 1;
					
			}
			
			if(requestCode == SELECTED_PICTURE)
			{
				
				
				Uri uri = data.getData();
				String[]projection={MediaStore.Images.Media.DATA};
				
				Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
				cursor.moveToFirst();
				
				int columnIndex = cursor.getColumnIndex(projection[0]);
				lokasi_gambar = cursor.getString(columnIndex);
				cursor.close();
				
				gambarku = BitmapFactory.decodeFile(lokasi_gambar);
				Drawable d = new BitmapDrawable(gambarku);
				pilih_gambar.setImageDrawable(d);
				
				gambar_dipilih=1;
			}
			
		}
	}
	
	
	
	private class UploadGambar extends AsyncTask<Void, Void, Void>
	{
		Bitmap gambar;
		String nama;
		
		public UploadGambar(Bitmap gambar, String nama)
		{
			
			showpDialog();
			this.gambar = gambar;
			this.nama = nama;
		}
		
	

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			gambar.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
			String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
			
			ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
			dataToSend.add(new BasicNameValuePair("gambar", encodedImage));
			dataToSend.add(new BasicNameValuePair("nama", nama));

			HttpParams httpRequestParams = getHttpRequestParams();
			HttpClient client = new DefaultHttpClient(httpRequestParams);
			//HttpPost post = new HttpPost("http://192.168.43.8/appstar/api/post_foto_support");
			HttpPost post = new HttpPost("http://appstar.esy.es/api/post_foto_support");
			
			
			try {
				post.setEntity(new UrlEncodedFormEntity(dataToSend));
				client.execute(post);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			hidepDialog();
			//urlku = "http://192.168.43.8/appstar/api/postHelp?kategori="+komen_kategori+"&id_post="+komen_id_post+"&id_user="+komen_id_user+"&komentar="+komen_komentar+"&gambar="+nama+".jpg";
        	urlku = "http://appstar.esy.es/api/postHelp?kategori="+komen_kategori+"&id_post="+komen_id_post+"&id_user="+komen_id_user+"&komentar="+komen_komentar+"&gambar="+nama+".jpg";
			post_komentar();
		}
		
		
	}
	
	private HttpParams getHttpRequestParams()
	{
		HttpParams httpRequestParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpRequestParams, 1000 * 30);
		HttpConnectionParams.setSoTimeout(httpRequestParams, 1000 * 30);
		return httpRequestParams;
	}
	
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Peringatan");
		dialogkeluar.setMessage("Komentar Harus Diisi");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Peringatan");
		dialogkeluar.setMessage("Gambar Harus Dipilih");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		/*Dataku data2 = Dataku.findById(Dataku.class, 1L);
		if((id_user[posisi].equals(data2.id_userku))||(get_id_pengapload.equals(data2.id_userku)))
		{
			
		}*/
	}
	
	
	
	private void dialog_opsi() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.model_opsi);
        
        Button edit = (Button) dialog.findViewById(R.id.opsi_edit);
        edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Intent intent = new Intent(Masalah.this,Post.class);
				intent.putExtra("kirimjudul", ""+get_judul);
				intent.putExtra("kirimdeskripsi", ""+get_deskripsi);
				intent.putExtra("kirimlokasi", ""+get_lokasi);
				intent.putExtra("kirimkategori", ""+get_kategori);
				intent.putExtra("kirimgambar", ""+get_gambar_masalah);
				intent.putExtra("kirimid", ""+get_id_post);
				startActivity(intent);
			}
		});
        Button hapus = (Button) dialog.findViewById(R.id.opsi_hapus);
        hapus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				dialog_hapus();
			}
		});
               
        dialog.show();

    }
	
	private void dialog_hapus() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.model_dialog_hapus);
        
        Button edit = (Button) dialog.findViewById(R.id.opsi_hapus_ya);
        edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				hapus_kiriman();
				Intent intent = new Intent(Masalah.this,Profile.class);
				startActivity(intent);
			}
		});
        Button hapus = (Button) dialog.findViewById(R.id.opsi_hapus_tidak);
        hapus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
               
        dialog.show();

    }
	
	
	public void hapus_kiriman()
	{
		
		//String url = "http://appstar.esy.es/api/delete_post?id_post=32&nama_gambar=61465310118.jpg;
		String url = "http://appstar.esy.es/api/delete_post?id_post="+id_kirim+"&nama_gambar="+get_gambar_masalah;
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                    	hidepDialog();
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
	}
	
	

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		ambil_komentar();
	}
	
	
	public Uri getLocalBitmapUri(ImageView imageView) {
	    // Extract Bitmap from ImageView drawable
	    Drawable drawable = imageView.getDrawable();
	    Bitmap bmp = null;
	    if (drawable instanceof BitmapDrawable){
	       bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
	    } else {
	       return null;
	    }
	    // Store image to default external storage directory
	    Uri bmpUri = null;
	    try {
	        // Use methods on Context to access package-specific directories on external storage.
	        // This way, you don't need to request external read/write permission.
	        // See https://youtu.be/5xVh-7ywKpE?t=25m25s
	        File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
	        FileOutputStream out = new FileOutputStream(file);
	        bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
	        out.close();
	        bmpUri = Uri.fromFile(file);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return bmpUri;
	}
	
	public void notif_share()
	{
		Dataku data2 = Dataku.findById(Dataku.class, 1L);
		
		String url_notif = "http://appstar.esy.es/api/notif_share?id_post="+get_id_post+"&id_user="+data2.id_userku;
		JsonArrayRequest req = new JsonArrayRequest(url_notif,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                            JSONObject respon = (JSONObject) response
                                    .get(0);
                            
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                        
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	/*VolleyLog.d(TAG, "Error: " + error.getMessage());
                    	hidepDialog();
                        Toast.makeText(Post.this,
                                error.getMessage(), Toast.LENGTH_LONG).show();*/
                        //Toast.makeText(Post.this, "ERROR", Toast.LENGTH_LONG).show();
                    	
                    	
                        
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
	}
}
