package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;


public class Petugas_Pencarian extends Fragment implements OnClickListener, OnRefreshListener, OnItemClickListener{

	ListView hasil_pencarian_masalah;
	private SwipeRefreshLayout swipeRefreshLayout;
	EditText pencarian;
	ImageButton tombol_cari;
	String cari="";
	private ProgressDialog pDialog;
	
	String[] judul;
	String[] gambar;
	String[] deskripsi;
	String[] id_masalah;
	String[] lokasi;
	String[] kategori;
	String[] cek;
	private static String TAG = Petugas_Verifikasi.class.getSimpleName();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		
		
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Mencari...");
        pDialog.setCancelable(false);
        
        View rootView = inflater.inflate(R.layout.activity_petugas__pencarian, container, false);
         
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.pencarian_masalah_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		
        hasil_pencarian_masalah = (ListView) rootView.findViewById(R.id.petugas_pencarian);
        hasil_pencarian_masalah.setOnItemClickListener(this);
        
        pencarian = (EditText) rootView.findViewById(R.id.teks_cari);
        tombol_cari = (ImageButton) rootView.findViewById(R.id.tbl_cari);
        tombol_cari.setOnClickListener(this);
        
        cari_data_petugas();
        
        return rootView;
    }

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		cari_data_petugas();
	}

	@Override
	public void onClick(View pilihan) {
		// TODO Auto-generated method stub
		switch (pilihan.getId()) {
		case R.id.tbl_cari:
			cari = pencarian.getText().toString();
			if(cari==null)
			{
				cari="";
			}
			cari_data_petugas();
			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
		// TODO Auto-generated method stub
		Dataku data = Dataku.findById(Dataku.class, 1L);
		String id_petugas = data.id_userku;
		String id_post = id_masalah[posisi];
		
		String nu = "null";
		if(cek[posisi].equals(nu))
		{
			String json_alamat = "http://appstar.esy.es/api/tanggapiPost?id_post="+id_post+"&id_petugas="+data.id_userku;
	        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
	                new Response.Listener<JSONArray>() {
	                    @Override
	                    public void onResponse(JSONArray response) {
	                    	
	                        Log.d(TAG, response.toString());
	                        
	                    }
	                }, new Response.ErrorListener() {
	                    @Override
	                    public void onErrorResponse(VolleyError error) {
	                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
	                 
	                    }
	                });
	        // Adding request to request queue
	        req.setRetryPolicy(new DefaultRetryPolicy(
					30000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
					));
	        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
	        requestQueue.add(req);
	        
	        onRefresh();
		}
		else
		{
			String json_alamat = "http://appstar.esy.es/api/batalkanTanggapiPost?id_petugas="+id_petugas+"&id_post="+id_post;
	        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
	                new Response.Listener<JSONArray>() {
	                    @Override
	                    public void onResponse(JSONArray response) {
	                    	
	                        Log.d(TAG, response.toString());
	                        
	                    }
	                }, new Response.ErrorListener() {
	                    @Override
	                    public void onErrorResponse(VolleyError error) {
	                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
	                 
	                    }
	                });
	        // Adding request to request queue
	        req.setRetryPolicy(new DefaultRetryPolicy(
					30000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
					));
	        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
	        requestQueue.add(req);
	        onRefresh();
		}
	}
  
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    public void cari_data_petugas()
    {
    	swipeRefreshLayout.setRefreshing(true);
    	Dataku data = Dataku.findById(Dataku.class, 1L);
    	String id_petugas = data.id_kategori_petugas;
    	
    	//String json_alamat = "http://192.168.43.8/appstar/api/search_problem_petugas?id_petugas="+id_petugas+"&cari="+cari;
		String json_alamat = "http://appstar.esy.es/api/search_problem_petugas?id_petugas="+id_petugas+"&cari="+cari;
        
		JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                            
                        	judul = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	deskripsi = new String[jumlah_data];
                        	id_masalah = new String[jumlah_data];
                        	lokasi = new String[jumlah_data];
                        	kategori = new String[jumlah_data];
                        	cek = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                                JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                judul[i] = hasil.getString("title");
                                gambar[i] = hasil.getString("image");
                                deskripsi[i] = hasil.getString("caption");
                                id_masalah[i] = hasil.getString("id_post");
                                lokasi[i] = hasil.getString("location_name");
                                kategori[i] = hasil.getString("nama_kategori");
                                cek[i] = hasil.getString("id_petugas");
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                          
                           swipeRefreshLayout.setRefreshing(false);
                           hidepDialog();
                        }
                                              
                        Adapter_Petugas_Hasil adapter = new Adapter_Petugas_Hasil(getActivity(), judul, gambar, deskripsi, lokasi, kategori, cek);
                        hasil_pencarian_masalah.setAdapter(adapter);
                      
                       
                        
                        adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                		hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	hidepDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(req);
        showpDialog();
    }
    
   
}