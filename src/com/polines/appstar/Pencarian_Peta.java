package com.polines.appstar;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


public class Pencarian_Peta extends Fragment implements LocationListener {

	WebView mWebView;
	private ProgressDialog pDialog;
	Double latitude,longitude;
	String latitudeku,longitudeku;
	LocationManager locationManager ;
    String provider;
    Location location;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading Peta");
        pDialog.setCancelable(true);
		showpDialog();
		
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_pencarian__peta, container, false);
         
        
        ambil_lokasi();
        
        
        mWebView = (WebView) rootView.findViewById(R.id.tampil_peta);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl("https://www.google.co.id/maps/@"+latitudeku+","+longitudeku+",17z?hl=en");
        
        mWebView.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			        hidepDialog();
			    }
			});
        
        return rootView;
    }
    
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    public void ambil_lokasi()
    {
    	// Getting LocationManager object
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
 
        // Creating an empty criteria object
        Criteria criteria = new Criteria();
 
        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);
 
        if(provider!=null && !provider.equals("")){
 
            // Get the location from the given provider
            location = locationManager.getLastKnownLocation(provider);
 
            locationManager.requestLocationUpdates(provider, 20000, 1, this);
 
            if(location!=null)
                onLocationChanged(location);
            else
                Toast.makeText(getActivity().getBaseContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();
 
        }else{
            Toast.makeText(getActivity().getBaseContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
        }
    }

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        latitudeku = String.valueOf(latitude);
        longitudeku = String.valueOf(longitude);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
   
}