package com.polines.appstar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.squareup.picasso.Picasso;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Post extends Activity implements OnClickListener, LocationListener{

	String pilihan,lokasi_gambar;
	String pilihan1="kamera";
	String pilihan2="galeri";
	private ProgressDialog pDialog;
	private static String TAG = MainActivity.class.getSimpleName();
	CameraPhoto kamera;
	final int CAMERA_REQUEST = 12345;
	
	int TAKE_PHOTO_CODE = 0,facebook=-1,twitter=-1,instagram=-1;
    public static int count = 0;
	private static final int SELECTED_PICTURE=1;

	ImageButton tombol_post,sharefacebook,sharetwitter,shareinstagram,pilih_gambar;
	EditText post_judul,post_deskripsi;
	TextView lokasi;
	
	String judulku="",deskripsiku="",latitudeku="",longitudeku="",kota,provinsi,negara;
	Double latitude,longitude;
	LocationManager locationManager ;
    String provider;
    Location location;
    
    Long timestamp;
	String timestampku;
	
	String up_user_id,up_gambar,up_judul,up_deskripsi,up_nama_lokasi="",up_deskripsi_lokasi,up_kategori_post;
	int gambar_dipilih=0,kategori_dipilih=0;
	String nama_gambar,url,url2,cek="";
	
	Bitmap gambarku;
	Button kategori;
	String[] kategoriku={"Lalu Lintas","Parkir","Transportasi","Lingkungan","Banjir","Ruang Terbuka Hijau","Perijinan","Lampu PJU","Kebakaran","Pelayanan Publik","Akses Wifi","CFD","Kebersihan","Pungli","Infrastruktur","Keamanan"};
	int jumlah_judul,jumlah_deskripsi;
	TextView coba;
	
	HttpPost post;
	String kirim_judul,kirim_deskripsi,kirim_lokasi,kirim_kategori,kirim_gambar,kirim_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_post);
		
		
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait");
        pDialog.setCancelable(false);
        
		pilih_gambar = (ImageButton) findViewById(R.id.pilih_gambar);
		pilih_gambar.setOnClickListener(this);
		
		tombol_post = (ImageButton) findViewById(R.id.tombol_post);
		tombol_post.setOnClickListener(this);
		sharefacebook = (ImageButton) findViewById(R.id.sharefacebook);
		sharefacebook.setOnClickListener(this);
		shareinstagram = (ImageButton)findViewById(R.id.shareinstagram);
		shareinstagram.setOnClickListener(this);
		sharetwitter=(ImageButton)findViewById(R.id.sharetwitter);
		sharetwitter.setOnClickListener(this);
		
		post_judul = (EditText) findViewById(R.id.post_judul);
		post_deskripsi = (EditText) findViewById(R.id.post_deskripsi);
		lokasi = (TextView)findViewById(R.id.post_lokasi);
		
		kategori = (Button) findViewById(R.id.pilih_kategori);
		kategori.setOnClickListener(this);
		
		/*Intent pilih = getIntent();
		pilihan = pilih.getStringExtra("pilihan");*/
		
		kamera = new CameraPhoto(getApplicationContext());
		
		/*timestamp = System.currentTimeMillis()/1000;
		timestampku = timestamp.toString();
		java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
		String date = dateFormat.format(new Date());
		String date2 = (DateFormat.format("dd-MM-yyyy hh:mm:ss", new java.util.Date()).toString());
		post_deskripsi.setText(""+date2);*/
		
		Intent baru = getIntent();
		kirim_judul = baru.getStringExtra("kirimjudul");
		kirim_deskripsi = baru.getStringExtra("kirimdeskripsi");
		kirim_gambar = baru.getStringExtra("kirimgambar");
		kirim_kategori = baru.getStringExtra("kirimkategori");
		kirim_lokasi = baru.getStringExtra("kirimlokasi");
		kirim_id = baru.getStringExtra("kirimid");
		if(kirim_judul==null)
		{
			ambil_lokasi();
		}
		else
		{
			post_judul.setText(""+kirim_judul);
			post_deskripsi.setText(""+kirim_deskripsi);
			lokasi.setText(""+kirim_lokasi);
			kategori.setText("Kategori : "+kirim_kategori);
			Picasso.with(this)
			.load("http://appstar.esy.es/uploads/post/"+kirim_gambar)
			.resize(600, 400)
	        .centerCrop()
	        .into(pilih_gambar);
		}
		
		
  	}


	@Override
	public void onClick(View pilihan) {
		switch (pilihan.getId()) {
		
		case R.id.pilih_gambar:
			dialog_pilihan();
		break;
		
		case R.id.tombol_post:
			
			judulku = post_judul.getText().toString();
			deskripsiku = post_deskripsi.getText().toString();
			judulku = judulku.replace(' ','+');
			deskripsiku = deskripsiku.replace(' ','+');
			
			jumlah_judul = judulku.length();
			jumlah_deskripsi = deskripsiku.length();
			
			Dataku data = Dataku.findById(Dataku.class, 1L);
    		String id_user = data.id_userku;
			
    		timestamp = System.currentTimeMillis()/1000;
			String timestampku = timestamp.toString();
			nama_gambar = id_user+timestampku;
			
			if(gambar_dipilih==1||kirim_judul!=null)
			{
				if((judulku.equals(cek))||(deskripsiku.equals(cek)))
				{
					peringatan3();
				}
				else {
					if(kategori_dipilih==1||kirim_judul!=null)
					{
						if((jumlah_judul<=1)||(jumlah_deskripsi<=1))
						{
							peringatan8();
						}
						else{
							if(kirim_judul==null)
							{
								//url = "http://192.168.43.8/appstar/api/post_masalah?user_id_user="+id_user+"&image="+nama_gambar+".jpg&title="+judulku+"&caption="+deskripsiku+"&latitude="+latitudeku+"&longitude="+longitudeku+"&location_name=semarang&location_description=Kosong&kategori="+up_kategori_post+"&lokasi="+up_nama_lokasi;
								url = "http://appstar.esy.es/api/post_masalah?user_id_user="+id_user+"&image="+nama_gambar+".jpg&title="+judulku+"&caption="+deskripsiku+"&latitude="+latitudeku+"&longitude="+longitudeku+"&location_name=semarang&location_description=Kosong&kategori="+up_kategori_post+"&lokasi="+up_nama_lokasi;
								new UploadGambar( gambarku, nama_gambar).execute();
							}
							else
							{
								
								
								url2 = "http://appstar.esy.es/api/update_masalah?id_post="+kirim_id+"&title="+judulku+"&caption="+deskripsiku;
								if(kategori_dipilih==1)
								{
									url2 = "http://appstar.esy.es/api/update_masalah?id_post="+kirim_id+"&title="+judulku+"&caption="+deskripsiku+"&kategori="+up_kategori_post;
								}
								if(gambar_dipilih==1)
								{
									nama_gambar = kirim_gambar;
									new UploadGambar( gambarku, nama_gambar).execute();
								}
								if(gambar_dipilih!=1)
								{
									edit_post();
								}
								
								
								
							}
							
						}
						
					}
					else
					{
						peringatan6();
					}
					
				}
				
			}
			else {
				peringatan2();
			}
			
			
			
			break;

		case R.id.sharefacebook:
			facebook=-facebook;
			if(facebook==-1)
			{
				sharefacebook.setImageResource(R.drawable.social1a);
			}
			if(facebook==1)
			{
				sharefacebook.setImageResource(R.drawable.social1b);
			}
		break;
		
		case R.id.shareinstagram:
			instagram=-instagram;
			if(instagram==-1)
			{
				shareinstagram.setImageResource(R.drawable.social3a);
			}
			if(instagram==1)
			{
				shareinstagram.setImageResource(R.drawable.social3b);
			}
		break;
		
		case R.id.sharetwitter:
			twitter=-twitter;
			if(twitter==-1)
			{
				sharetwitter.setImageResource(R.drawable.social2a);
			}
			if(twitter==1)
			{
				sharetwitter.setImageResource(R.drawable.social2b);
			}
		break;
		
		case R.id.pilih_kategori:
			showRadioButtonDialog();
		break;
		}
		
	}
	
	
	public void dialog_pilihan()
	{
		AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Pilih Gambar");
		dialogkeluar.setNegativeButton ("Ambil Gambar", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				pilih_kamera();
			}
		});
		dialogkeluar.setPositiveButton("Galeri", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				pilih_galeri();
			}
		});
		dialogkeluar.show();
	}
	
	
	public void pilih_kamera()
	{
		try {
			startActivityForResult(kamera.takePhotoIntent(),CAMERA_REQUEST);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), "Ada Yang Salah Saat Mengambil Foto", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void pilih_galeri()
	{
		/*startActivityForResult(galeri.openGalleryIntent(),GALERI_REQUEST);*/
		Intent galeri = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galeri, SELECTED_PICTURE);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK)
		{
			
			if(requestCode == CAMERA_REQUEST)
			{
				lokasi_gambar = kamera.getPhotoPath();
				/*try {*/
					
					gambarku = BitmapFactory.decodeFile(lokasi_gambar);
					Drawable d = new BitmapDrawable(gambarku);
					pilih_gambar.setImageDrawable(d);
					gambar_dipilih = 1;
					
					/*Bitmap bitmap = ImageLoader.init().from(photopath).requestSize(512, 512).getBitmap();
					
					pilih_gambar.setImageBitmap(bitmap);*/
				/*} catch (FileNotFoundException e) {
					Toast.makeText(getApplicationContext(), "Load gambar gagal", Toast.LENGTH_SHORT).show();
				}*/
			}
			
			if(requestCode == SELECTED_PICTURE)
			{
				
				
				Uri uri = data.getData();
				String[]projection={MediaStore.Images.Media.DATA};
				
				Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
				cursor.moveToFirst();
				
				int columnIndex = cursor.getColumnIndex(projection[0]);
				lokasi_gambar = cursor.getString(columnIndex);
				cursor.close();
				
				gambarku = BitmapFactory.decodeFile(lokasi_gambar);
				Drawable d = new BitmapDrawable(gambarku);
				pilih_gambar.setImageDrawable(d);
				
				gambar_dipilih=1;
			}
			
		}
	}
	
	public void ambil_lokasi()
    {
    	// Getting LocationManager object
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
 
        // Creating an empty criteria object
        Criteria criteria = new Criteria();
 
        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);
 
        if(provider!=null && !provider.equals("")){
 
            // Get the location from the given provider
            location = locationManager.getLastKnownLocation(provider);
 
            locationManager.requestLocationUpdates(provider, 20000, 1, this);
 
            if(location!=null)
            {
                onLocationChanged(location);
            }
            else
            {
                Toast.makeText(this.getBaseContext(), "Aktifkan GPS Anda", Toast.LENGTH_SHORT).show();
                peringatan();
            }
        }else{
            Toast.makeText(this.getBaseContext(), "Aktifkan GPS Anda", Toast.LENGTH_SHORT).show();
        }
    }


	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		latitude = location.getLatitude();
        longitude = location.getLongitude();
        latitudeku = String.valueOf(latitude);
        longitudeku = String.valueOf(longitude);
        
       
        Geocoder geocoded = new Geocoder(getApplicationContext(), Locale.getDefault());   
        try {
            List<Address> addresses = geocoded.getFromLocation(
                    latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);

                StringBuilder sb = new StringBuilder(128);
                sb.append(" ");
                sb.append(address.getAddressLine(0));
                if (addresses.size() > 1) {
                    sb.append(", ");
                    sb.append(address.getAddressLine(1));
                } 
                if (addresses.size() > 2) {
                    sb.append(", ");
                    sb.append(address.getAddressLine(2));
                }
                
                if (!TextUtils.isEmpty(address.getLocality())) {
                    if (sb.length() > 0) {
                        sb.append(", ");
                    }  
                    sb.append(address.getLocality());
                }

              String strAddress = sb.toString();
  	          up_nama_lokasi = strAddress;    
  	          lokasi.setText(""+strAddress);
            }
            else
            {
            	ambil_lokasi2();
            }
        } catch (Exception ex) {
        	ambil_lokasi2();
        }
        
                
	}


	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Peringatan");
		dialogkeluar.setMessage("Lokasi Tidak Diketahui. Aktifkan GPS dan Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Post");
		dialogkeluar.setMessage("Pilih Gambar Terlebih Dahulu");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan3()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Post");
		dialogkeluar.setMessage("Judul dan Deskripsi Tidak Boleh Kosong");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan4()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Error");
		dialogkeluar.setMessage("Cek Koneksi Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan5()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Posting");
		dialogkeluar.setMessage("Posting Masalah Berhasil");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				Intent intent = new Intent(Post.this,Profile.class);
				startActivity(intent);
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan6()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Peringatan");
		dialogkeluar.setMessage("Kategori Masalah Harus Dipilih");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan7()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Posting");
		dialogkeluar.setMessage("Posting Masalah Gagal");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				Intent intent = new Intent(Post.this,Profile.class);
				startActivity(intent);
				
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan8()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Posting");
		dialogkeluar.setMessage("Judul Dan Deskripsi Minimal 10 Karakter");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	
	
	private class UploadGambar extends AsyncTask<Void, Void, Void>
	{
		Bitmap gambar;
		String nama;
		
		public UploadGambar(Bitmap gambar, String nama)
		{
			
			showpDialog();
			this.gambar = gambar;
			this.nama = nama;
		}
		
	

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			gambar.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
			String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
			
			ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
			dataToSend.add(new BasicNameValuePair("gambar", encodedImage));
			dataToSend.add(new BasicNameValuePair("nama", nama));

			HttpParams httpRequestParams = getHttpRequestParams();
			HttpClient client = new DefaultHttpClient(httpRequestParams);
			//post = new HttpPost("http://192.168.43.8/appstar/api/post_foto");
			if(kirim_judul==null)
			{
				post = new HttpPost("http://appstar.esy.es/api/post_foto");
			}
			else
			{
				post = new HttpPost("http://appstar.esy.es/api/edit_foto");
			}
			
			
			
			try {
				post.setEntity(new UrlEncodedFormEntity(dataToSend));
				client.execute(post);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(kirim_judul==null)
			{
				post();
			}
			else
			{
				edit_post();
			}
			
			//ubah_profil();
		}
		
		
	}
	
	private HttpParams getHttpRequestParams()
	{
		HttpParams httpRequestParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpRequestParams, 1000 * 30);
		HttpConnectionParams.setSoTimeout(httpRequestParams, 1000 * 30);
		return httpRequestParams;
	}
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
    public void post()
	{
    	
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject respon = (JSONObject) response
                                        .get(0);
                               
                               String berhasil = "berhasil";
                               String pesan = respon.getString("pesan");
                               if(pesan.equals(berhasil))
                               {
                            	   peringatan5();
                               }
                               else{
                            	   peringatan7();
                               }
                               
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            hidepDialog();
                        }
                        
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        /*Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();*/
                        hidepDialog();
                        //peringatan4();
                    }
                });
        // Adding request to request queue
				req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
		RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
   
    }
    
    private void showRadioButtonDialog() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.model_dialog_kategori);
        List<String> stringList=new ArrayList<String>();  // here is list 
        for(int i=0;i<16;i++) {
            stringList.add(kategoriku[i]);
        }
        RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);

            for(int i=0;i<stringList.size();i++){
                RadioButton rb=new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
                //rb.setId(R.id.);
                rb.setText(stringList.get(i));
                rb.setTextColor(Color.rgb(63, 169, 217));
                rg.addView(rb);
            }
            
            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                     int childCount = group.getChildCount();
                     for (int x = 0; x < childCount; x++) {
                        RadioButton btn = (RadioButton) group.getChildAt(x);
                        if (btn.getId() == checkedId) {
                        	 kategori_dipilih = 1;
                             //Log.e("selected RadioButton->",btn.getText().toString());
                             kategori.setText("Kategori : "+btn.getText().toString());
                             int pilihan = x+1;
                             up_kategori_post = String.valueOf(pilihan);
                             dialog.dismiss();

                        }
                     }
                }
            });

        dialog.show();

    }
    
    
    public void ambil_lokasi2()
    {
    	
    	//String lokasiku = "http://192.168.43.8/appstar/api/lokasi?lat="+latitudeku+"&long="+longitudeku;
		String lokasiku = "http://appstar.esy.es/api/lokasi?lat="+latitudeku+"&long="+longitudeku;
    	JsonArrayRequest req = new JsonArrayRequest(lokasiku,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject data = (JSONObject) response
                                        .get(0);
                                
                                up_nama_lokasi = data.getString("lokasi");
                                lokasi.setText(""+up_nama_lokasi);
                                up_nama_lokasi = up_nama_lokasi.replace(' ', '+');
                        } catch (JSONException e) {
                            e.printStackTrace();
                            
                        }
 
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        
                    }
                });
        // Adding request to request queue
    	req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
		RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
    }
    
    public void edit_post()
    {
    	JsonArrayRequest req = new JsonArrayRequest(url2,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                            JSONObject respon = (JSONObject) response
                                    .get(0);
                            
                            hidepDialog();
                            
                            Intent intent = new Intent(Post.this,Masalah.class);
	                        intent.putExtra("datakirim", ""+kirim_id);
	                        startActivity(intent);
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hidepDialog();
                    }
                        
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	/*VolleyLog.d(TAG, "Error: " + error.getMessage());
                    	hidepDialog();
                        Toast.makeText(Post.this,
                                error.getMessage(), Toast.LENGTH_LONG).show();*/
                        //Toast.makeText(Post.this, "ERROR", Toast.LENGTH_LONG).show();
                    	
                    	
                        
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
    }
    
}
