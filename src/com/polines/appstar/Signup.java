package com.polines.appstar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends Activity implements OnClickListener{

	ImageButton signup;
	Button login;
	EditText nama,username,email,alamat,password,password2;
	String namaku,usernameku,emailku,alamatku,passwordku,password2ku,cek="",cek2="berhasil",cek3="gagal",avatar,id_user;
	private static String TAG = MainActivity.class.getSimpleName();
	String json_alamat,pesan;
	private ProgressDialog pDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_signup);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Signing Up");
        pDialog.setCancelable(false);
        
        signup = (ImageButton) findViewById(R.id.tbl_signup);
        signup.setOnClickListener(this);
        nama = (EditText) findViewById(R.id.signup_nama);
        username = (EditText)findViewById(R.id.signup_username);
        email = (EditText)findViewById(R.id.signup_email);
        alamat = (EditText) findViewById(R.id.signup_alamat);
        password = (EditText) findViewById(R.id.signup_password);
        password2 = (EditText) findViewById(R.id.signup_password2);
        
        login = (Button) findViewById(R.id.signup_login);
        login.setOnClickListener(this);
	}

	
	@Override
	public void onClick(View pilihan) {
		switch (pilihan.getId()) {
		case R.id.tbl_signup:
			
			namaku = nama.getText().toString();
			namaku = namaku.replace(' ', '+');
			usernameku = username.getText().toString();
			emailku = email.getText().toString();
			alamatku = alamat.getText().toString();
			alamatku = alamatku.replace(' ','+');
			passwordku = password.getText().toString();
			password2ku = password2.getText().toString();
			
			if((namaku.equals(cek))||(usernameku.equals(cek))||(emailku.equals(cek))||(alamatku.equals(cek))||(passwordku.equals(cek))||password2ku.equals(cek))
			{
				peringatan();
				
			}
			
			else if(!passwordku.equals(password2ku))
			{
				peringatan2();
			}
			
			else
			{
				//json_alamat = "http://192.168.43.8/appstar/api/signup?&fullname="+namaku+"&username="+usernameku+"&email="+emailku+"&address="+alamatku+"&password="+passwordku+"&register=true";
				json_alamat = "http://appstar.esy.es/api/signup?&fullname="+namaku+"&username="+usernameku+"&email="+emailku+"&address="+alamatku+"&password="+passwordku+"&register=true";
				
				fungsi_signup();
				//signup();
				//postData();
			}
			
			
			break;
			
		case R.id.signup_login:
			Intent baru = new Intent(Signup.this,Halamanlogin.class);
			startActivity(baru);
		break;

		}
		
	}
	
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Isikan Semua Data");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Password Harus Sama");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
    }
	
	public void peringatan3()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Username atau Email Sudah Ada");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
	
	public void peringatan4()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("SignUp");
		dialogkeluar.setMessage("SignUp Berhasil, Silahkan Login Untuk Melanjutkan");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
	
	public void peringatan5()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("SignUp Gagal");
		dialogkeluar.setMessage("Cek Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
	
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
    public void fungsi_signup()
	{
    	
		JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject respon = (JSONObject) response
                                        .get(0);
                                
                                pesan = respon.getString("pesan");
                               
                                if(pesan.equals(cek2))
                                {
                                	nama.setText("");
                                	username.setText("");
                                	email.setText("");
                                	alamat.setText("");
                                	password.setText("");
                                	password2.setText("");
                            		
                            		peringatan4();
                                }
                                if(pesan.equals(cek3))
                                {
                                	peringatan3();
                                }
                               
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            /*Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                            peringatan5();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        /*Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
                        peringatan5();
                    }
                });
        // Adding request to request queue
		req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
		RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
        
        
    }
    

    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	}
    	
    	
    
   

}
