package com.polines.appstar;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class Pengaturan_profil extends Activity implements OnClickListener{

	EditText fullname,username,email,alamat,password;
	String alamat_gambar,lokasi_gambar="",nama,passwordlama,passwordbaru;
	ImageView gambar_profil;
	CheckBox chek;
	ImageButton ganti;
	Button ganti_foto,ganti_password,logout;
	Bitmap gambar_untuk_upload;
	String result,pesanberhasil="berhasil",pesan,usernamelama,emaillama;
	
	int foto_diganti=0,password_diganti=0;
	
	String url,up_id,up_fullname,up_username,up_password,up_email,up_alamat,up_avatar;
	
	private static final int SELECTED_PICTURE=1;
	private static String TAG = MainActivity.class.getSimpleName();
	private ProgressDialog pDialog;
	private ProgressDialog pDialog2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_pengaturan_profil);
		
		fullname = (EditText) findViewById(R.id.pengaturan_profil_fullname);
		username = (EditText) findViewById(R.id.pengaturan_profil_username);
		email =(EditText) findViewById(R.id.pengaturan_profil_email);
		alamat = (EditText) findViewById(R.id.pengaturan_profil_alamat);
		/*password = (EditText) findViewById(R.id.pengaturan_profil_password);
		password2 = (EditText) findViewById(R.id.pengaturan_profil_password2);*/
		
		Dataku data = Dataku.findById(Dataku.class, 1L);
		fullname.setText(""+data.fullnameku);
		username.setText(""+data.usernameku);
		email.setText(""+data.emailku);
		alamat.setText(""+data.alamatku);
		usernamelama = data.usernameku;
		emaillama = data.emailku;
		
		//alamat_gambar = "http://192.168.43.8/appstar/uploads/avatar/"+data.avatarku;
		alamat_gambar = "http://appstar.esy.es/uploads/avatar/"+data.avatarku;
		
		gambar_profil = (ImageView) findViewById(R.id.pengaturan_profil_gambar);
		
		Picasso.with(this)
		.load(alamat_gambar)
		.memoryPolicy(MemoryPolicy.NO_CACHE)
		.networkPolicy(NetworkPolicy.NO_CACHE)
		.transform(new CircleTransform())
		.resize(300, 300)
        .centerCrop()
        .into(gambar_profil);
		
		/*chek = (CheckBox) findViewById(R.id.pengaturan_check);*/
		
		ganti = (ImageButton) findViewById(R.id.pengaturan_ganti);
		ganti.setOnClickListener(this);
		ganti_foto = (Button) findViewById(R.id.pengaturan_profil_ganti_foto);
		ganti_foto.setOnClickListener(this);
		logout = (Button) findViewById(R.id.logout);
		logout.setOnClickListener(this);
		ganti_password = (Button) findViewById(R.id.pengaturan_profil_ganti_password);
		ganti_password.setOnClickListener(this);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait");
        pDialog.setCancelable(false);
        
        pDialog2 = new ProgressDialog(this);
        pDialog2.setMessage("Logging Out...");
        pDialog2.setCancelable(false);
	}


	@Override
	public void onClick(View pilihan) {
		switch (pilihan.getId()) {
		case R.id.pengaturan_ganti:
			
			Dataku data3 = Dataku.findById(Dataku.class, 1L);
			
			up_id = data3.id_userku;
			up_fullname = fullname.getText().toString();
			up_fullname = up_fullname.replace(' ', '+');
			up_username = username.getText().toString();
			up_email = email.getText().toString();
			up_alamat = alamat.getText().toString();
			up_alamat = up_alamat.replace(' ', '+');
			nama = data3.usernameku+".JPG";
			
			
			
			if(password_diganti==1)
			{
				up_password = passwordbaru;
			}
			else
			{
				up_password = data3.passwordku;
			}
			
			if(foto_diganti==1)
			{
				up_avatar = nama;
				//url = "http://192.168.43.8/appstar/api/editprofile?&id_user="+up_id+"&fullname="+up_fullname+"&username="+up_username+"&email="+up_email+"&alamat="+up_alamat+"&avatar="+up_avatar+"&password="+up_password+"&old_username="+usernamelama+"&old_email="+emaillama;
				url = "http://appstar.esy.es/api/editprofile?&id_user="+up_id+"&fullname="+up_fullname+"&username="+up_username+"&email="+up_email+"&alamat="+up_alamat+"&avatar="+up_avatar+"&password="+up_password+"&old_username="+usernamelama+"&old_email="+emaillama;
				new UploadGambar( gambar_untuk_upload, nama).execute();
			}
			else
			{
				up_avatar = data3.avatarku;
				//url = "http://192.168.43.8/appstar/api/editprofile?&id_user="+up_id+"&fullname="+up_fullname+"&username="+up_username+"&email="+up_email+"&alamat="+up_alamat+"&avatar="+up_avatar+"&password="+up_password+"&old_username="+usernamelama+"&old_email="+emaillama;
				url = "http://appstar.esy.es/api/editprofile?&id_user="+up_id+"&fullname="+up_fullname+"&username="+up_username+"&email="+up_email+"&alamat="+up_alamat+"&avatar="+up_avatar+"&password="+up_password+"&old_username="+usernamelama+"&old_email="+emaillama;
				ubah_profil();
			}
			
			
			

			break;

	
		case R.id.pengaturan_profil_ganti_foto:
			pilih_galeri();
			break;
			
		case R.id.pengaturan_profil_ganti_password:
			dialog_password1();
			break;
			
		case R.id.logout:
			showpDialog2();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					Dataku data = Dataku.findById(Dataku.class, 1L);
					data.validasiku="0";
					data.id_kategori_petugas="null";
					data.save();
					hidepDialog2();
					Intent intent = new Intent(Pengaturan_profil.this,Halamanlogin.class);
					startActivity(intent);
				}
			},2000);	
		break;
		}
		
	}
	
	
	
	public void pilih_galeri()
	{
		/*startActivityForResult(galeri.openGalleryIntent(),GALERI_REQUEST);*/
		Intent galeri = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(galeri, SELECTED_PICTURE);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK)
		{

			if(requestCode == SELECTED_PICTURE)
			{
				
				
				Uri uri = data.getData();
				String[]projection={MediaStore.Images.Media.DATA};
				
				Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
				cursor.moveToFirst();
				
				int columnIndex = cursor.getColumnIndex(projection[0]);
				lokasi_gambar = cursor.getString(columnIndex);
				cursor.close();
				
				gambar_untuk_upload = BitmapFactory.decodeFile(lokasi_gambar);
				
				gambar_profil = (ImageView)findViewById(R.id.pengaturan_profil_gambar);
				Picasso.with(this)
				.load("file://" + lokasi_gambar)
				.noPlaceholder()
				.transform(new CircleTransform())
				.resize(300, 300)
		        .centerCrop()
		        .into(gambar_profil);
				
				foto_diganti=1;
				

			}
			
		}
	}
	
	
	private class UploadGambar extends AsyncTask<Void, Void, Void>
	{
		Bitmap gambar;
		String nama;
		
		public UploadGambar(Bitmap gambar, String nama)
		{
			
			showpDialog();
			this.gambar = gambar;
			this.nama = nama;
		}
		
	

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			gambar.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
			String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
			
			ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
			dataToSend.add(new BasicNameValuePair("gambar", encodedImage));
			dataToSend.add(new BasicNameValuePair("nama", nama));

			HttpParams httpRequestParams = getHttpRequestParams();
			HttpClient client = new DefaultHttpClient(httpRequestParams);
			//HttpPost post = new HttpPost("http://192.168.43.8/appstar/api/ubahfoto");
			HttpPost post = new HttpPost("http://appstar.esy.es/api/ubahfoto");
			
			
			try {
				post.setEntity(new UrlEncodedFormEntity(dataToSend));
				client.execute(post);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			hidepDialog();
			ubah_profil();
		}
		
		
	}
	
	private HttpParams getHttpRequestParams()
	{
		HttpParams httpRequestParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpRequestParams, 1000 * 30);
		HttpConnectionParams.setSoTimeout(httpRequestParams, 1000 * 30);
		return httpRequestParams;
	}
	

	
	
	public void dialog_password1() {
	    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
	    LayoutInflater inflater = this.getLayoutInflater();
	    final View dialogView = inflater.inflate(R.layout.model_popup_password, null);
	    dialogBuilder.setView(dialogView);

	    password = (EditText) dialogView.findViewById(R.id.edit_password);

	    dialogBuilder.setTitle("PASSWORD");
	    dialogBuilder.setMessage("Masukkan Password Lama Anda");
	    dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            passwordlama = password.getText().toString();
	            passwordlama = md5(passwordlama);
	            
	            Dataku data2 = Dataku.findById(Dataku.class, 1L);
	            String pass = data2.passwordku;
	            if(passwordlama.equals(pass))
	            {
	            	dialog.dismiss();
	            	dialog_password2();
	            }
	            else
	            {
	            	dialog.dismiss();
	            	password_error();
	            }

	        }
	    });
	    dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            dialog.dismiss();
	        }
	    });
	    AlertDialog b = dialogBuilder.create();
	    b.show();
	}
	
	public void dialog_password2() {
	    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
	    LayoutInflater inflater = this.getLayoutInflater();
	    final View dialogView = inflater.inflate(R.layout.model_popup_password, null);
	    dialogBuilder.setView(dialogView);

	    password = (EditText) dialogView.findViewById(R.id.edit_password);

	    dialogBuilder.setTitle("PASSWORD");
	    dialogBuilder.setMessage("Masukkan Password Baru Anda");
	    dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            passwordbaru = password.getText().toString();
	            passwordbaru = md5(passwordbaru);
	            password_diganti=1;
	            ganti_password.setText("Password Telah Diganti");
	            dialog.dismiss();
	        }
	    });
	    dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            dialog.dismiss();
	        }
	    });
	    AlertDialog b = dialogBuilder.create();
	    b.show();
	}
	
	public void password_error()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Password Yang Anda Masukkan Salah");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	
	//Encrypt Ke MD5
	private String md5(String in) {
	    MessageDigest digest;
	    try {
	        digest = MessageDigest.getInstance("MD5");
	        digest.reset();
	        digest.update(in.getBytes());
	        byte[] a = digest.digest();
	        int len = a.length;
	        StringBuilder sb = new StringBuilder(len << 1);
	        for (int i = 0; i < len; i++) {
	            sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
	            sb.append(Character.forDigit(a[i] & 0x0f, 16));
	        }
	        return sb.toString();
	    } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
	    return null;
	}
	
	
	public void ubah_profil()
	{
    	
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject respon = (JSONObject) response
                                        .get(0);
                                
                                pesan = respon.getString("pesan");
                                if(pesan.equals(pesanberhasil))
                                {
                                	peringatan2();
                                }
                                else
                                {
                                	peringatan3();
                                }
                               
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            
                            peringatan();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        /*Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();*/
                        hidepDialog();
                        peringatan();
                    }
                });
        // Adding request to request queue
		req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
		RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
   
    }
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    private void showpDialog2() {
        if (!pDialog2.isShowing())
            pDialog2.show();
    }
 
    private void hidepDialog2() {
        if (pDialog2.isShowing())
            pDialog2.dismiss();
    }
    
    public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Data Gagal Diproses, Cek Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    
    public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("EDIT PROFILE");
		dialogkeluar.setMessage("Data Berhasil Disimpan");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				Dataku data = Dataku.findById(Dataku.class, 1L);
				up_fullname = up_fullname.replace('+', ' ');
				data.fullnameku = up_fullname;
				data.usernameku = up_username;
				data.emailku = up_email;
				up_alamat = up_alamat.replace('+', ' ');
				data.alamatku = up_alamat;
				data.passwordku = up_password;
				if(foto_diganti==1)
				{
					data.avatarku = up_avatar+".JPG";
				}
				else
				{
					data.avatarku = up_avatar;
				}
				
				data.save();
				dialog.dismiss();
				Intent profil = new Intent(Pengaturan_profil.this,Profile.class);
				startActivity(profil);
				
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    
    public void peringatan3()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage(""+pesan);
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
	

}
