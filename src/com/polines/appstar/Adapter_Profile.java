package com.polines.appstar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter_Profile extends ArrayAdapter<String> {

	String[] judul={};
	String[] gambar={};
	String[] deskripsi={};
	String[] id_petugas={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Profile(Context context, String[]judul, String[]gambar, String[]deskripsi, String[]id_petugas) {
		super(context, R.layout.model_pencarian_masalah,judul);
		
		this.c = context;
		this.judul = judul;
		this.gambar = gambar;
		this.deskripsi = deskripsi;
		this.id_petugas = id_petugas;
		
	}
	
	public class ViewHolder
	{
		TextView judul ;
		ImageView gambar;
		TextView deskripsi;
		ImageView icon;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_pencarian_masalah,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		holder.judul = (TextView) convertView.findViewById(R.id.teks_pencarian_masalah_judul);
		holder.gambar = (ImageView) convertView.findViewById(R.id.gambar_pencarian_masalah);
		holder.judul.setText(judul[position]);
		//holder.judul.setText(id_petugas[position]);
		holder.deskripsi = (TextView) convertView.findViewById(R.id.teks_pencarian_masalah_deskripsi);
		holder.deskripsi.setText(deskripsi[position]);
		holder.icon = (ImageView) convertView.findViewById(R.id.icon_pencarian_masalah);
		
		String cek = "null";
		if(id_petugas[position].equals(cek))
		{
			holder.icon.setImageResource(R.drawable.no_masalah);
		}
		else
		{
			holder.icon.setImageResource(R.drawable.yes_masalah);
		}
		
		
		Picasso.with(getContext())
		//.load("http://192.168.43.8/appstar/uploads/post/"+gambar[position])
		.load("http://appstar.esy.es/uploads/post/"+gambar[position])
		.resize(300, 300)
        .centerCrop()
        .into(holder.gambar);
		
		return convertView;
	}
	
	

}