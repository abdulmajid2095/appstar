package com.polines.appstar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter_notifikasi extends ArrayAdapter<String> {
	
	
	String[] kategori={};
	String[] gambar={};
	String[] name={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_notifikasi(Context context, String[]kategori, String[]gambar, String[]name) {
		super(context, R.layout.model_notif,kategori);
		
		this.c = context;
		this.kategori = kategori;
		this.gambar = gambar;
		this.name = name;
		
	}
	
	public class ViewHolder
	{
		TextView judulku ;
		ImageView gambar;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String kategori1="1";
		String kategori2="2";
		String kategori3="3";
		String kategori4="4";
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_notif,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		holder.judulku = (TextView) convertView.findViewById(R.id.teks_notif);
		holder.gambar = (ImageView) convertView.findViewById(R.id.gambar_notif);
		
		if(kategori[position].equals(kategori1))
		{
			holder.judulku.setText(name[position]+" mulai berlangganan kepada anda");
			
			Picasso.with(getContext())
			.load("http://appstar.esy.es/uploads/avatar/"+gambar[position])
			//.load("http://192.168.43.8/appstar/uploads/avatar/"+gambar[position])
			.transform(new CircleTransform())
			.resize(300, 300)
	        .centerCrop()
	        .into(holder.gambar);
		}
		
		if(kategori[position].equals(kategori2))
		{
			holder.judulku.setText(name[position]+" mengomentari kiriman anda");
			
			Picasso.with(getContext())
			.load("http://appstar.esy.es/uploads/post/"+gambar[position])
			//.load("http://192.168.43.8/appstar/post/avatar/"+gambar[position])
			.resize(300, 300)
	        .centerCrop()
	        .into(holder.gambar);
		}
		if(kategori[position].equals(kategori3))
		{
			holder.judulku.setText(name[position]+" membagikan kiriman anda");
			
			Picasso.with(getContext())
			.load("http://appstar.esy.es/uploads/post/"+gambar[position])
			//.load("http://192.168.43.8/appstar/post/avatar/"+gambar[position])
			.resize(300, 300)
	        .centerCrop()
	        .into(holder.gambar);
		}
		if(kategori[position].equals(kategori4))
		{
			holder.judulku.setText(name[position]+" memberikan bantuan kepada kriman anda");
			
			Picasso.with(getContext())
			.load("http://appstar.esy.es/uploads/post/"+gambar[position])
			//.load("http://192.168.43.8/appstar/post/avatar/"+gambar[position])
			.resize(300, 300)
	        .centerCrop()
	        .into(holder.gambar);
		}
		
		return convertView;
	}
	

}
