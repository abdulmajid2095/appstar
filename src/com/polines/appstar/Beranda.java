package com.polines.appstar;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v4.app.NotificationCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Beranda extends Activity implements OnClickListener, OnItemClickListener, OnRefreshListener{

	String[] kategoriku={"Semua","Lalu Lintas","Parkir","Transportasi","Lingkungan","Banjir","Ruang Terbuka Hijau","Perijinan","Lampu PJU","Kebakaran","Pelayanan Publik","Akses Wifi","CFD","Kebersihan","Pungli","Infrastruktur","Keamanan"};
	Button menu1,menu2,menu3,menu4,menu5;
	int limit=10;
	ListView timeline;
	private static String TAG = Beranda.class.getSimpleName();
	String[] judul;
	String[] lokasi;
	String[] waktu;
	String[] gambar;
	String[] pengapload;
	String[] kategori;
	String[] validasi;
	String[] id_post;
	String[] nama_pengapload;
	private ProgressDialog pDialog;
	Adapter_beranda adapter;
	Button loadmore;
	private SwipeRefreshLayout swipeRefreshLayout;
	ImageButton tbl_kategori;
	String kategori_dipilih="0";
	String semua="0";
	String json_alamat;
	//notif
	int notif_baru,notif_lama,notif_tampil,notif_sementara=0;
	ImageView notif_round;
	TextView notif_jumlah;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_beranda);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Mencari...");
        pDialog.setCancelable(false);
		
		menu1=(Button) findViewById(R.id.menu11);
		menu1.setOnClickListener(this);
		menu2=(Button) findViewById(R.id.menu21);
		menu2.setOnClickListener(this);
		menu3=(Button) findViewById(R.id.menu31);
		menu3.setOnClickListener(this);
		menu4=(Button) findViewById(R.id.menu41);
		menu4.setOnClickListener(this);
		menu5=(Button) findViewById(R.id.menu51);
		menu5.setOnClickListener(this);
		
		tbl_kategori=(ImageButton) findViewById(R.id.beranda_tbl_kategori);
		tbl_kategori.setOnClickListener(this);
		
		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.beranda_swipe_refresh_layout);
		swipeRefreshLayout.setOnRefreshListener(this);
		
		timeline = (ListView) findViewById(R.id.timeline);
		timeline.setOnItemClickListener(this);
		View myFooterView = getLayoutInflater().inflate(R.layout.footer_beranda,timeline,false);
		timeline.addFooterView(myFooterView,null,false);
		
		loadmore = (Button) myFooterView.findViewById(R.id.beranda_loadmore);
		loadmore.setOnClickListener(this);
		ambildata();
		
		//Notif
		timer_notif();
	}

	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
			case R.id.menu11:
				Intent beranda = new Intent(Beranda.this, Beranda.class);
				startActivity(beranda);
			break;
			
			case R.id.menu21:
				Intent pencarian = new Intent(Beranda.this, Pencarian.class);
				startActivity(pencarian);
			break;
			
			case R.id.menu31:
			
				Intent post = new Intent(Beranda.this, Post.class);
				startActivity(post);
			break;
			
			case R.id.menu41:
				Intent notifikasi = new Intent(Beranda.this, Notifikasi.class);
				startActivity(notifikasi);
			break;
			
			case R.id.menu51:
				Intent profile = new Intent(Beranda.this, Profile.class);
				startActivity(profile);
			break;
			
			case R.id.beranda_loadmore:
				limit = limit+10;
				ambildata();
			break;
			
			case R.id.beranda_tbl_kategori:
				pilihan_kategori();
			break;
		}
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	}
	
	
	private void ambildata() {
		
		
		swipeRefreshLayout.setRefreshing(true);
		
		if(kategori_dipilih.equals(semua))
		{
			//String json_alamat = "http://192.168.43.8/appstar/api/beranda?limit="+limit;
			json_alamat = "http://appstar.esy.es/api/beranda?limit="+limit;
			/*Toast.makeText(Beranda.this,
			        "Masuk Semua : "+kategori_dipilih, Toast.LENGTH_LONG).show();*/
		}
		else
		{
			/*Toast.makeText(Beranda.this,
			        "Masuk Kategori : "+kategori_dipilih, Toast.LENGTH_LONG).show();*/
			//String json_alamat = "http://192.168.43.8/appstar/api/beranda?limit="+limit+"&kategori="+kategori_dipilih;
			json_alamat = "http://appstar.esy.es/api/beranda_kategori?limit="+limit+"&kategori="+kategori_dipilih;
		}
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                            
                        	judul = new String[jumlah_data];
                        	lokasi = new String[jumlah_data];
                        	waktu = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	pengapload = new String[jumlah_data];
                        	kategori = new String[jumlah_data];
                        	validasi = new String[jumlah_data];
                        	id_post = new String[jumlah_data];
                        	nama_pengapload = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                               JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	judul[i] = hasil.getString("title");
                                	lokasi[i] = hasil.getString("location_name");
                                    gambar[i] = hasil.getString("image");
                                    waktu[i] = hasil.getString("create_date");
                                    pengapload[i] = hasil.getString("avatar");
                                    kategori[i] = hasil.getString("kategori_post_id_kategori_post");
                                    validasi[i] = hasil.getString("id_petugas");
                                    id_post[i] = hasil.getString("id_post");
                                    nama_pengapload[i] = hasil.getString("username");
                        	}
                        	
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                           //peringatan2();
                        }
                        if(judul[0].equals("null"))
                        {
                        	Toast.makeText(getApplicationContext(),
                                    "Masalah Tidak Ditemukan Pada Kategori Ini",
                                    Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                        	adapter = new Adapter_beranda(getApplicationContext(), judul, lokasi, waktu, gambar, pengapload, kategori, validasi, nama_pengapload);
                    		timeline.setAdapter(adapter);
                    		timeline.setOnItemClickListener(new OnItemClickListener() {

    							@Override
    							public void onItemClick(AdapterView<?> arg0,
    									View arg1, int posisi, long arg3) {
    								// TODO Auto-generated method stub
    								Intent intent = new Intent(Beranda.this,Masalah.class);
    								intent.putExtra("datakirim", ""+id_post[posisi]);
    								startActivity(intent);
    								
    							}
    						});
                    		
                        }
                        
                        adapter.notifyDataSetChanged();
                		swipeRefreshLayout.setRefreshing(false);
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	error.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                                "Error: " + error.getMessage(),
                                Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(req);
        
    }
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		ambildata();
		
		
	}
	
	
	public void timer_notif()
	{
		final Handler ha=new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                cek_notif();
            	ha.postDelayed(this, 8000);
            }
        }, 5000);
	}
	
	public void cek_notif() {
		Dataku data = Dataku.findById(Dataku.class, 1L);
		String id_ku = data.id_userku;
		//String json_alamat = "http://192.168.43.8/appstar/api/jumlah_notif?id_user="+id_ku;
		String json_alamat = "http://appstar.esy.es/api/jumlah_notif?id_user="+id_ku;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                            JSONObject respon = (JSONObject) response
                                    .get(0);
                            
                            String get_jumlah_baru = respon.getString("jumlah_baru");
                            notif_baru = Integer.parseInt(get_jumlah_baru);
                            String get_jumlah_lama = respon.getString("jumlah_lama");
                            notif_lama = Integer.parseInt(get_jumlah_lama);
                            
                            if(notif_baru>notif_lama)
                            {
                            	if(notif_baru>notif_sementara)
                            	{
                            		notif_sementara = notif_baru;
                            		notif_tampil = notif_baru-notif_lama;
                            		/*Toast.makeText(Beranda.this,
                                            ""+notif_tampil, Toast.LENGTH_LONG).show();*/
                                	notifikasi();
                            	}
                            }
                            
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                        /*Toast.makeText(Beranda.this,
                                error.getMessage(), Toast.LENGTH_LONG).show();*/
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }
	
	public void notifikasi()
	{
		String kirim = String.valueOf(notif_tampil);
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Intent intent = new Intent(Beranda.this, Notifikasi.class);
		intent.putExtra("notif", ""+notif_tampil);
		intent.putExtra("notif2", ""+notif_baru);
		int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
		PendingIntent pIntent = PendingIntent.getActivity(this, uniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification mNotification = new NotificationCompat.Builder(this)
		.setContentTitle("Appstar")
		.setContentText(notif_tampil+" Buah Pemberitahuan Baru")
		.setSmallIcon(R.drawable.logo)
		.setContentIntent(pIntent)
		.setSound(soundUri)
		.addAction(R.drawable.g1, "View", pIntent)
		.addAction(0, "Remind", pIntent)
		.setAutoCancel(true)
		.build();
	
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(0, mNotification);

	
	}
	
	
	private void pilihan_kategori() {

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.model_dialog_kategori);
        List<String> stringList=new ArrayList<String>();  // here is list 
        for(int i=0;i<16;i++) {
            stringList.add(kategoriku[i]);
        }
        RadioGroup rg = (RadioGroup) dialog.findViewById(R.id.radio_group);

            for(int i=0;i<stringList.size();i++){
                RadioButton rb=new RadioButton(this); // dynamically creating RadioButton and adding to RadioGroup.
                //rb.setId(R.id.);
                rb.setText(stringList.get(i));
                rb.setTextColor(Color.rgb(63, 169, 217));
                rg.addView(rb);
            }
            
            rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                     int childCount = group.getChildCount();
                     for (int x = 0; x < childCount; x++) {
                        RadioButton btn = (RadioButton) group.getChildAt(x);
                        if (btn.getId() == checkedId) {
                        	 //kategori_dipilih = 1;
                             //Log.e("selected RadioButton->",btn.getText().toString());
                             //kategori_dipilih=btn.getText().toString();
                        	int pilihan = x; 
                        	kategori_dipilih=String.valueOf(pilihan);
                            
                             //up_kategori_post = String.valueOf(pilihan);
                             ambildata();
                             dialog.dismiss();

                        }
                     }
                }
            });

        dialog.show();

    }

   
}
