package com.polines.appstar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter_Pencarian_User extends ArrayAdapter<String> {

	String[] user={};
	String[] gambar={};
	String[] fullname={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Pencarian_User(Context context, String[]user, String[]gambar, String[]fullname) {
		super(context, R.layout.model_pencarian_user,user);
		
		this.c = context;
		this.user = user;
		this.gambar = gambar;
		this.fullname = fullname;
	}
	
	public class ViewHolder
	{
		TextView userku ;
		ImageView gambar;
		TextView fullname;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_pencarian_user,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		holder.userku = (TextView) convertView.findViewById(R.id.teks_pencarian_user_username);
		holder.gambar = (ImageView) convertView.findViewById(R.id.gambar_pencarian_user);
		holder.userku.setText(user[position]);
		holder.fullname = (TextView) convertView.findViewById(R.id.teks_pencarian_user_fullname);
		holder.fullname.setText(fullname[position]);
		
		
		
		Picasso.with(getContext())
		.load("http://appstar.esy.es/uploads/avatar/"+gambar[position])
		//.load("http://192.168.43.8/appstar/uploads/avatar/"+gambar[position])
		.memoryPolicy(MemoryPolicy.NO_CACHE)
		.networkPolicy(NetworkPolicy.NO_CACHE)
		.transform(new CircleTransform())
		.resize(300, 300)
        .centerCrop()
        .into(holder.gambar);
		
		return convertView;
	}
	

}
