package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Profile extends Activity implements OnClickListener,OnItemClickListener{
	
	private static String TAG = Profile.class.getSimpleName();
	
	ListView kiriman;
	
	ImageButton pengaturan_profil;
	Button menu1,menu2,menu3,menu4,menu5,pelanggan,berlangganan,follow;
	ImageView gambar_profil;
	String alamat_gambar,id_user,avatar;
	JSONObject hasil;
	int jumlah_data,i;
	String[] judul;
	String[] gambar;
	String[] deskripsi;
	String[] id_masalah;
	String[] id_petugas;
	String json_alamat;
	int j_pelanggan=0,j_langganan=0,status_follow;
	
	String id_ku,idkirim,userlain_id="",userlain_username="",userlain_fullname="",userlain_email="",userlain_alamat="",userlain_avatar="";
	
	TextView username,fullname,email,alamat,jumlah_post,jumlah_pelanggan,jumlah_berlangganan;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_profile);

		Intent baru = getIntent();
		idkirim = baru.getStringExtra("datakirim");
		
		menu1=(Button) findViewById(R.id.menu15);
		menu1.setOnClickListener(this);
		menu2=(Button) findViewById(R.id.menu25);
		menu2.setOnClickListener(this);
		menu3=(Button) findViewById(R.id.menu35);
		menu3.setOnClickListener(this);
		menu4=(Button) findViewById(R.id.menu45);
		menu4.setOnClickListener(this);
		menu5=(Button) findViewById(R.id.menu55);
		menu5.setOnClickListener(this);
		follow = (Button) findViewById(R.id.follow);
		follow.setOnClickListener(this);
		pengaturan_profil = (ImageButton) findViewById(R.id.pengaturan_profil);
		pengaturan_profil.setOnClickListener(this);
		
		
		kiriman = (ListView) findViewById(R.id.kiriman);
		kiriman.setOnItemClickListener(this);
		View myHeaderView = getLayoutInflater().inflate(R.layout.header_profile,kiriman,false);
		kiriman.addHeaderView(myHeaderView,null,false);
		kiriman.setAdapter(null);
		
		pelanggan = (Button) findViewById(R.id.tbl_pelanggan);
		pelanggan.setOnClickListener(this);
		berlangganan = (Button) findViewById(R.id.tbl_berlangganan);
		berlangganan.setOnClickListener(this);
		
		username = (TextView) findViewById(R.id.profil_username);
		fullname = (TextView) findViewById(R.id.profil_fullname);
		alamat = (TextView)findViewById(R.id.profil_alamat);
		email = (TextView) findViewById(R.id.profil_email);
		
		jumlah_post = (TextView) findViewById(R.id.profile_jumlah_post);
		jumlah_pelanggan = (TextView) findViewById(R.id.profile_jumlah_pelanggan);
		jumlah_berlangganan = (TextView) findViewById(R.id.profile_jumlah_berlangganan);
		
		Dataku data = Dataku.findById(Dataku.class, 1L);
		
		if(idkirim!=null)
		{
			if(data.id_userku.equals(idkirim))
			{
				set_user();
			}
			else
			{
				pengaturan_profil.setVisibility(View.INVISIBLE);
				cek_follow();
				ambil_informasi();
				id_user = idkirim;
			}
		}
		
		else
		{
			set_user();
		}
		
		
		
		jumlah();
		ambildata();
		
	}

	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
		
			case R.id.menu15:
				Intent beranda = new Intent(Profile.this, Beranda.class);
				startActivity(beranda);
			break;
			
			case R.id.menu25:
				Intent pencarian = new Intent(Profile.this, Pencarian.class);
				startActivity(pencarian);
			break;
			
			case R.id.menu35:
				Intent post = new Intent(Profile.this, Post.class);
				startActivity(post);
			break;
			
			case R.id.menu45:
				Intent notifikasi = new Intent(Profile.this, Notifikasi.class);
				startActivity(notifikasi);
			break;
			
			case R.id.menu55:
				Intent profile = new Intent(Profile.this, Profile.class);
				startActivity(profile);
			break;
			
			case R.id.pengaturan_profil:
				Intent pengaturan = new Intent(Profile.this, Pengaturan_profil.class);
				startActivity(pengaturan);
			break;
			
			case R.id.tbl_pelanggan:
				if(j_pelanggan==0)
				{
					
				}
				else
				{
					Intent intent = new Intent(Profile.this,Pelanggan.class);
					intent.putExtra("datakirim", "Pelanggan");
					intent.putExtra("datakirim2", ""+id_user);
					startActivity(intent);
				}
				
			break;
			
			case R.id.tbl_berlangganan:
				if(j_langganan==0)
				{
					
				}
				else
				{
					Intent intent2 = new Intent(Profile.this,Pelanggan.class);
					intent2.putExtra("datakirim", "Berlangganan");
					intent2.putExtra("datakirim2", ""+id_user);
					startActivity(intent2);
				}
				
			break;
			
			case R.id.follow:
				
				status_follow=-status_follow;
				if(status_follow==-1)
				{
					follow.setText("Berlangganan");
					follow.setBackgroundColor(Color.rgb(252, 73, 73));
				}
				if(status_follow==1)
				{
					follow.setText("Berhenti Berlangganan");
					follow.setBackgroundColor(Color.rgb(72, 207, 72));
				}
				follow();
				
			break;
		}
		
	}
	
	
	public void jumlah()
	{ 
    	//String url = "http://192.168.43.8/appstar/api/pengguna?id_user="+id_user;
    	String url = "http://appstar.esy.es/api/pengguna?id_user="+id_user;
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject respon = (JSONObject) response
                                        .get(0);
                               
                               String berlangganan = respon.getString("berlangganan");
                               String pelanggan = respon.getString("pelanggan");
                               String post = respon.getString("post");
                               
                               jumlah_post.setText(""+post);
                               jumlah_pelanggan.setText(""+pelanggan);
                               jumlah_berlangganan.setText(""+berlangganan);
                               j_pelanggan = Integer.parseInt(pelanggan);
                               j_langganan = Integer.parseInt(berlangganan);
                               
                               //username.setText(""+j_langganan);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                    
                    }
                });
        // Adding request to request queue
				req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
		RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
   
    }
	
	
	
	private void ambildata() {
		//json_alamat = "http://192.168.43.8/appstar/api/post_pengguna?id_user="+id_user;
		json_alamat = "http://appstar.esy.es/api/post_pengguna?id_user="+id_user;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	jumlah_data = response.length();
                            
                            
                        	judul = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	deskripsi = new String[jumlah_data];
                        	id_masalah = new String[jumlah_data];
                        	id_petugas = new String[jumlah_data];
                        	
                        	for (i = 0; i < response.length(); i++) {
                        	
                                hasil = (JSONObject) response
                                        .get(i);
                                
                                	judul[i] = hasil.getString("title");
                                    gambar[i] = hasil.getString("image");
                                    deskripsi[i] = hasil.getString("caption");
                                    id_masalah[i] = hasil.getString("id_post");
                                    id_petugas[i] = hasil.getString("id_petugas");
                                    
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                           
                        }
                        Adapter_Profile adapter = new Adapter_Profile(getApplicationContext(), judul, gambar, deskripsi,id_petugas);
                        kiriman.setAdapter(adapter);
                      
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        
                    	
                        
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }
	
	
	private void ambil_informasi() {
		//json_alamat = "http://192.168.43.8/appstar/api/ambil_data_user?id_user="+idkirim;
		json_alamat = "http://appstar.esy.es/api/ambil_data_user?id_user="+idkirim;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                            JSONObject respon = (JSONObject) response
                                    .get(0);
                            
                            userlain_id = respon.getString("id_user");
                            userlain_username = respon.getString("username");
                            userlain_fullname = respon.getString("fullname");
                            userlain_email = respon.getString("email");
                            userlain_alamat = respon.getString("address");
                            userlain_avatar = respon.getString("avatar");
                            
                            
                            username.setText(""+userlain_username);
                			fullname.setText(""+userlain_fullname);
                			alamat.setText("Alamat : "+userlain_alamat);
                			email.setText("Email : "+userlain_email);
                			id_user = userlain_id;
                			avatar = userlain_avatar;
                			
                			alamat_gambar = "http://appstar.esy.es/uploads/avatar/"+avatar;
                			//alamat_gambar = "http://192.168.43.8/appstar/uploads/avatar/"+avatar;
                			gambar_profil = (ImageView)findViewById(R.id.profil_picture);
                			Picasso.with(getApplicationContext())
                			.load(alamat_gambar)
                			.memoryPolicy(MemoryPolicy.NO_CACHE)
                			.networkPolicy(NetworkPolicy.NO_CACHE)
                			.transform(new CircleTransform())
                			.resize(300, 300)
                	        .centerCrop()
                	        .into(gambar_profil);
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        
                    }
                        
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(Profile.this,
                                error.getMessage(), Toast.LENGTH_LONG).show();
                    	peringatan();
                        
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        
    }
	
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Cek Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	
	public void cek_follow()
	{
		Dataku data = Dataku.findById(Dataku.class, 1L);
		id_ku = data.id_userku;
		
		//json_alamat = "http://192.168.43.8/appstar/api/cek_follow?id_ku="+id_ku+"&id_userlain="+idkirim;
		json_alamat = "http://appstar.esy.es/api/cek_follow?id_user="+id_ku+"&id_userlain="+idkirim;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                            JSONObject respon = (JSONObject) response
                                    .get(0);
                            
                            String follow_yes="yes";
                            String follow_no = "no";
                            
                            String hasil_follow = respon.getString("follow");
                            LinearLayout.LayoutParams paramsbtn = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
                			follow.setLayoutParams(paramsbtn);
                            if(hasil_follow.equals(follow_yes))
                            {
                            	status_follow=1;
                            	follow.setText("Berhenti Berlangganan");
                				follow.setBackgroundColor(Color.rgb(72, 207, 72));
                            }
                            if(hasil_follow.equals(follow_no))
                            {
                            	status_follow=-1;
                            	follow.setText("Berlangganan");
                				follow.setBackgroundColor(Color.rgb(252, 73, 73));
                            }
                            
                            
                    } catch (JSONException e) {
                        e.printStackTrace();
                        
                    }
                        
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(Profile.this,
                                error.getMessage(), Toast.LENGTH_LONG).show();
                    	peringatan();
                        
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
	}
	
	public void follow()
	{
		Dataku data = Dataku.findById(Dataku.class, 1L);
		id_ku = data.id_userku;
		
		//json_alamat = "http://192.168.43.8/appstar/api/follow?id_ku="+id_ku+"&id_userlain="+idkirim;
		json_alamat = "http://appstar.esy.es/api/follow?id_user="+id_ku+"&id_userlain="+idkirim;
        JsonArrayRequest req = new JsonArrayRequest(json_alamat,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	VolleyLog.d(TAG, "Error: " + error.getMessage());
                 
                    }
                });
        // Adding request to request queue
        req.setRetryPolicy(new DefaultRetryPolicy(
				30000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
				));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
	}
	
	public void set_user()
	{
		Dataku data = Dataku.findById(Dataku.class, 1L);
		follow.setVisibility(View.INVISIBLE);
		
		username.setText(""+data.usernameku);
		fullname.setText(""+data.fullnameku);
		alamat.setText("Alamat : "+data.alamatku);
		email.setText("Email : "+data.emailku);
		id_user = data.id_userku;
		avatar = data.avatarku;
		
		alamat_gambar = "http://appstar.esy.es/uploads/avatar/"+avatar;
		//alamat_gambar = "http://192.168.43.8/appstar/uploads/avatar/"+avatar;
		gambar_profil = (ImageView)findViewById(R.id.profil_picture);
		Picasso.with(this)
		.load(alamat_gambar)
		.memoryPolicy(MemoryPolicy.NO_CACHE)
		.networkPolicy(NetworkPolicy.NO_CACHE)
		.transform(new CircleTransform())
		.resize(300, 300)
        .centerCrop()
        .into(gambar_profil);
	}

	@Override
	public void onItemClick(AdapterView arg0, View arg1, int posisi, long arg3) {

		Intent intent = new Intent(Profile.this,Masalah.class);
		intent.putExtra("datakirim", ""+id_masalah[posisi-1]);
		startActivity(intent);
		
	}


}
