package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class Halamanlogin extends Activity implements OnClickListener{
	
	private static String TAG = MainActivity.class.getSimpleName();
	ImageButton login;
	Button signup;
	EditText kotak_email,kotak_password;
	String dari_kotak_email,dari_kotak_password;
	String alamat_json;
	private ProgressDialog pDialog;
	int jumlah;
	String validasi,valid0="0",valid1="1",id_user,fullname,username,email,avatar,alamat_user,password,id_kategori_petugas,cek="";
	TextView rubah;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_halamanlogin);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Logging In");
        pDialog.setCancelable(false);
        
        login = (ImageButton) findViewById(R.id.tbl_login);
        login.setOnClickListener(this);
        
        signup = (Button) findViewById(R.id.login_daftar);
        signup.setOnClickListener(this);
        
        kotak_email = (EditText) findViewById(R.id.login_email);
        kotak_password = (EditText) findViewById(R.id.login_password);
        
        rubah = (TextView) findViewById(R.id.rubah);
	}

	

	@Override
	public void onClick(View pilihan) {
		switch (pilihan.getId()) {
		case R.id.tbl_login:
			
			dari_kotak_email = kotak_email.getText().toString();
			dari_kotak_password = kotak_password.getText().toString();
			
			//alamat_json = "http://192.168.43.8/appstar/api/login/?email="+dari_kotak_email+"&password="+dari_kotak_password;
			alamat_json = "http://appstar.esy.es/api/login/?email="+dari_kotak_email+"&password="+dari_kotak_password;
			
			if((dari_kotak_email.equals(cek)) || (dari_kotak_password.equals(cek)))
			{
				peringatan3();
			}
			else
			{
				cek_login();
			}
			
			break;
			
		case R.id.login_daftar:
			Intent baru = new Intent(Halamanlogin.this,Signup.class);
			startActivity(baru);
			break;

		}
		
	}
	
	public void cek_login()
	{
		JsonArrayRequest req = new JsonArrayRequest(alamat_json,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject login = (JSONObject) response
                                        .get(0);
                                
                                validasi = login.getString("validasi");
                                if (validasi.equals(valid0)){        
                                 	peringatan();
                                 }
                                if (validasi.equals(valid1)){     
                                	
                                	String cek = "3";
                                	String cek2 = "2";
                                	
                                	id_user = login.getString("id_user");
                                 	fullname = login.getString("fullname");
                                 	username = login.getString("username");
                                 	email = login.getString("email");
                                 	avatar = login.getString("avatar");
                                 	alamat_user = login.getString("address");
                                 	password = login.getString("password");
                                 	id_kategori_petugas = login.getString("id_kategori_petugas");
                                 	
                                	String level = login.getString("level");
                                	if(level.equals(cek))
                                	{
                                		
                                     	Dataku data = Dataku.findById(Dataku.class, 1L);
                                		data.validasiku = validasi;
                                		data.id_userku = id_user;
                                		data.fullnameku = fullname;
                                		data.usernameku = username;
                                		data.emailku = email;
                                		data.avatarku = avatar;
                                		data.alamatku = alamat_user;
                                		data.passwordku = password;
                                		data.save();
                                     	
                                     	Intent baru = new Intent(Halamanlogin.this,Beranda.class);
                            			startActivity(baru);
                                	}
                                	else if(level.equals(cek2))
                                	{
                                		
                            			Dataku data = Dataku.findById(Dataku.class, 1L);
                                		data.validasiku = "0";
                                		data.id_userku = id_user;
                                		data.fullnameku = fullname;
                                		data.usernameku = username;
                                		data.emailku = email;
                                		data.avatarku = avatar;
                                		data.alamatku = alamat_user;
                                		data.passwordku = password;
                                		data.id_kategori_petugas = id_kategori_petugas;
                                		data.save();
                                		
                                		Intent baru = new Intent(Halamanlogin.this,Halaman_Petugas.class);
                            			startActivity(baru);
                                	}
                                	
                                	
                                 	
                                }
                               
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            /*Toast.makeText(this,
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                            hidepDialog();
                            peringatan2();
                        }
 
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        /*Toast.makeText(this,
                                error.getMessage(), Toast.LENGTH_SHORT).show();*/
                        hidepDialog();
                        peringatan2();
                    }
                });
        // Adding request to request queue
		RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
        showpDialog();
        
        
    }
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
    public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Login Gagal");
		dialogkeluar.setMessage("Cek Email Atau Password Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    
    public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Login Error");
		dialogkeluar.setMessage("Cek Sambungan Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }

    public void peringatan3()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Login");
		dialogkeluar.setMessage("Email dan Password Tidak Boleh Kosong");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	}

}
