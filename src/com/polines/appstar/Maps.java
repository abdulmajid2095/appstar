package com.polines.appstar;

import java.util.ArrayList;

import org.osmdroid.bonuspack.location.NominatimPOIProvider;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.bonuspack.overlays.FolderOverlay;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.bonuspack.routing.RoadNode;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

public class Maps extends Activity implements LocationListener{
	
	private MapView         mMapView;
    private MapController   mMapController;
    Double latitude,longitude;
	String latitudeku,longitudeku;
	LocationManager locationManager ;
    String provider,lat_kirim,long_kirim;
    Location location;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_maps);

        Intent baru = getIntent();
		lat_kirim = baru.getStringExtra("latitude");
		long_kirim = baru.getStringExtra("longitude");
		
		if(lat_kirim==null)
		{
			ambil_lokasi();
			 if(latitude==null)
		        {
		        	latitude = -6.7968875;
		        	longitude = 110.5877585;
		        	peringatan();
		        }
		}
		else
		{
			latitude = Double.parseDouble(lat_kirim);
			longitude = Double.parseDouble(long_kirim);
		}
        
        mMapView = (MapView) findViewById(R.id.petaku);
        mMapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mMapController = (MapController) mMapView.getController();
        mMapController.setZoom(15);
        GeoPoint startPoint = new GeoPoint(latitude, longitude);
        mMapController.setCenter(startPoint);
        
        
        if(lat_kirim==null)
        {
        	Marker startMarker = new Marker(mMapView);
            startMarker.setPosition(startPoint);
            startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            //startMarker.setIcon(getResources().getDrawable(R.drawable.alam2));
            startMarker.setTitle("Kamu Disini Pengguna Appstar");
            mMapView.getOverlays().add(startMarker);
        }
        else
        {
        	Marker startMarker = new Marker(mMapView);
            startMarker.setPosition(startPoint);
            startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            //startMarker.setIcon(getResources().getDrawable(R.drawable.alam2));
            startMarker.setTitle("Lokasi Masalah Ada Disini");
            mMapView.getOverlays().add(startMarker);
        }
        
        
        /*//RoadManager roadManager = new OSRMRoadManager();
        RoadManager roadManager = new MapQuestRoadManager("aj5xs9j8K7u4JjUeyyz0QRD3XTn1Rv0x");
        roadManager.addRequestOption("routeType=bicycle");
        ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
        waypoints.add(startPoint);
        GeoPoint endPoint = new GeoPoint(-6.7968875, 110.5877585);
        waypoints.add(endPoint);*/
        
        /*Road road = roadManager.getRoad(waypoints);
        Polyline roadOverlay = RoadManager.buildRoadOverlay(road, this);
        mMapView.getOverlays().add(roadOverlay);
        mMapView.invalidate();
        
        Drawable nodeIcon = getResources().getDrawable(R.drawable.marker_node);
        for (int i=0; i<road.mNodes.size(); i++){
            RoadNode node = road.mNodes.get(i);
            Marker nodeMarker = new Marker(mMapView);
            nodeMarker.setPosition(node.mLocation);
            nodeMarker.setIcon(nodeIcon);
            nodeMarker.setTitle("Step "+i);
            mMapView.getOverlays().add(nodeMarker);
            
            nodeMarker.setSnippet(node.mInstructions);
            nodeMarker.setSubDescription(Road.getLengthDurationText(node.mLength, node.mDuration));
            Drawable icon = getResources().getDrawable(R.drawable.ic_continue);
            nodeMarker.setImage(icon);
        }*/
        
        //POI
        NominatimPOIProvider poiProvider = new NominatimPOIProvider();
        ArrayList<POI> pois = poiProvider.getPOICloseTo(startPoint, "cinema", 50, 0.1);
        FolderOverlay poiMarkers = new FolderOverlay(this);
        mMapView.getOverlays().add(poiMarkers);
        
        Drawable poiIcon = getResources().getDrawable(R.drawable.marker_poi_default);
        for (POI poi:pois){
            Marker poiMarker = new Marker(mMapView);
            poiMarker.setTitle(poi.mType);
            poiMarker.setSnippet(poi.mDescription);
            poiMarker.setPosition(poi.mLocation);
            poiMarker.setIcon(poiIcon);
            if (poi.mThumbnail != null){
                poiMarker.setImage(new BitmapDrawable(poi.mThumbnail));
            }
            poiMarkers.add(poiMarker);
        }
        
   }
    
    
    public void ambil_lokasi()
    {
    	// Getting LocationManager object
        locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
 
        // Creating an empty criteria object
        Criteria criteria = new Criteria();
 
        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);
 
        if(provider!=null && !provider.equals("")){
 
            // Get the location from the given provider
            location = locationManager.getLastKnownLocation(provider);
 
            locationManager.requestLocationUpdates(provider, 20000, 1, this);
 
            if(location!=null)
                onLocationChanged(location);
            else
                Toast.makeText(this.getBaseContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();
            	peringatan();
        }else{
            Toast.makeText(this.getBaseContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
        }
    }

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        latitudeku = String.valueOf(latitude);
        longitudeku = String.valueOf(longitude);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Peringatan");
		dialogkeluar.setMessage("Lokasi Tidak Diketahui. Aktifkan GPS dan Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }

}