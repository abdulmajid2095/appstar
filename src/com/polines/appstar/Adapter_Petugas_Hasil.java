package com.polines.appstar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Adapter_Petugas_Hasil extends ArrayAdapter<String> {
	
	String[] judul={};
	String[] gambar={};
	String[] deskripsi={};
	String[] lokasi={};
	String[] kategori={};
	String[] cek={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_Petugas_Hasil(Context context, String[]judul, String[]gambar, String[]deskripsi, String[]lokasi, String[]kategori, String[]cek) {
		super(context, R.layout.model_pencarian_masalah,judul);
		
		this.c = context;
		this.judul = judul;
		this.gambar = gambar;
		this.deskripsi = deskripsi;
		this.lokasi = lokasi;
		this.kategori = kategori;
		this.cek = cek;
	}
	
	public class ViewHolder
	{
		TextView judul ;
		ImageView gambar;
		TextView deskripsi,lokasi,kategori,status;
		LinearLayout verifikasi;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_petugas,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		holder.judul = (TextView) convertView.findViewById(R.id.adapater_petugas_judul);
		holder.judul.setText(judul[position]);
		
		holder.deskripsi = (TextView) convertView.findViewById(R.id.adapater_petugas_deskripsi);
		holder.deskripsi.setText(deskripsi[position]);
		
		holder.lokasi = (TextView) convertView.findViewById(R.id.adapater_petugas_lokasi);
		holder.lokasi.setText("Lokasi : "+lokasi[position]);
		
		holder.kategori = (TextView) convertView.findViewById(R.id.adapater_petugas_kategori);
		holder.kategori.setText("Kategori : "+kategori[position]);
		
		holder.gambar = (ImageView) convertView.findViewById(R.id.adapater_petugas_gambar);
		Picasso.with(getContext())
		.load("http://appstar.esy.es/uploads/post/"+gambar[position])
		//.load("http://192.168.43.8/appstar/uploads/post/"+gambar[position])
		.resize(300, 300)
        .centerCrop()
        .into(holder.gambar);
		
		holder.verifikasi = (LinearLayout) convertView.findViewById(R.id.adapater_petugas_layout);
		holder.status = (TextView) convertView.findViewById(R.id.adapater_petugas_status);
		String nu = "null";
		if(cek[position].equals(nu))
		{
			holder.status.setText("Tanggapi");
			holder.verifikasi.setBackgroundColor(Color.rgb(210, 51, 51));
		}
		else
		{
			holder.status.setText("Batalkan");
			holder.verifikasi.setBackgroundColor(Color.rgb(72, 207, 72));
		}
		
		
		return convertView;
	}
	
	

}
