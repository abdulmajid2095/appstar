package com.polines.appstar;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class Halaman_Petugas extends FragmentActivity implements OnClickListener{

	TextView username;
	ImageButton tombol_pengaturan;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_halaman__petugas);
		
		
		ViewPager pager = (ViewPager) findViewById(R.id.tab_petugas);
        FragmentManager fm = getSupportFragmentManager();
        Adapter_Petugas_Tab pagerAdapter = new Adapter_Petugas_Tab(fm);
        pager.setAdapter(pagerAdapter);
        
        username = (TextView) findViewById(R.id.petugas_username);
        Dataku data = Dataku.findById(Dataku.class, 1L);
        username.setText(""+data.usernameku);
        tombol_pengaturan = (ImageButton) findViewById(R.id.pengaturan_petugas);
        tombol_pengaturan.setOnClickListener(this);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
	}

	@Override
	public void onClick(View pilihan) {
		// TODO Auto-generated method stub
		switch (pilihan.getId()) {
		case R.id.pengaturan_petugas:
			Intent pengaturan = new Intent(Halaman_Petugas.this, Pengaturan_profil.class);
			startActivity(pengaturan);
		break;
		}
		
	}

	
}
