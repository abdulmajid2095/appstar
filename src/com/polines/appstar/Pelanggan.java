package com.polines.appstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Pelanggan extends Activity implements OnClickListener,OnItemClickListener{

	ListView hasil;
	Button menu1,menu2,menu3,menu4,menu5;
	String judul,pelanggan="Pelanggan",berlangganan="Berlangganan",id_userku;
	TextView judulku;
	
	String[] user;
	String[] gambar;
	String[] fullname;
	String [] id_user;
	
	private static String TAG = Pelanggan.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_pelanggan);
		
		menu1=(Button) findViewById(R.id.menu14);
		menu1.setOnClickListener(this);
		menu2=(Button) findViewById(R.id.menu24);
		menu2.setOnClickListener(this);
		menu3=(Button) findViewById(R.id.menu34);
		menu3.setOnClickListener(this);
		menu4=(Button) findViewById(R.id.menu44);
		menu4.setOnClickListener(this);
		menu5=(Button) findViewById(R.id.menu54);
		menu5.setOnClickListener(this);
		
		
		Intent baru = getIntent();
		judul = baru.getStringExtra("datakirim");  
		id_userku = baru.getStringExtra("datakirim2");
		
		judulku = (TextView) findViewById(R.id.pelanggan_judul);
		judulku.setText(""+judul);
		hasil = (ListView) findViewById(R.id.list_pelanggan);
		hasil.setOnItemClickListener(this);
		
		if(judul.equals(pelanggan))
		{
			ambil_pelanggan();
		}
		else
		{
			ambil_berlangganan();
		}
		
	}

	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
			case R.id.menu14:
				Intent beranda = new Intent(Pelanggan.this, Beranda.class);
				startActivity(beranda);
			break;
			
			case R.id.menu24:
				Intent pencarian = new Intent(Pelanggan.this, Pencarian.class);
				startActivity(pencarian);
			break;
			
			case R.id.menu34:
				Intent post = new Intent(Pelanggan.this, Post.class);
				startActivity(post);
			break;
			
			case R.id.menu44:
				Intent notifikasi = new Intent(Pelanggan.this, Notifikasi.class);
				startActivity(notifikasi);
			break;
			
			case R.id.menu54:
				Intent profile = new Intent(Pelanggan.this, Profile.class);
				startActivity(profile);
			break;
		}
		
	}
	
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("ERROR");
		dialogkeluar.setMessage("Pencarian gagal, Cek Sambungan Internet Anda");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
				
			}
		});
		dialogkeluar.show();
    }
	
	public void ambil_pelanggan()
	{
		//String url = "http://192.168.43.8/appstar/api/pelanggan?id_user="+id_userku;
		String url = "http://appstar.esy.es/api/pelanggan?id_user="+id_userku;
		
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                            
                        	user = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	fullname = new String[jumlah_data];
                        	id_user = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                               JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	user[i] = hasil.getString("username");
                                    gambar[i] = hasil.getString("avatar");
                                    fullname[i] = hasil.getString("fullname");
                                    id_user[i] = hasil.getString("id_user");
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                           
                        }
                        
                        Adapter_Pencarian_User adapter = new Adapter_Pencarian_User(Pelanggan.this, user, gambar, fullname);
                		hasil.setAdapter(adapter);
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
                    	
                    	peringatan();
                    	
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
	}
	
	
	
	public void ambil_berlangganan()
	{
		//String url = "http://192.168.43.8/appstar/api/berlangganan?id_user="+id_userku;
		String url = "http://appstar.esy.es/api/berlangganan?id_user="+id_userku;
		
		JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    	
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	int jumlah_data = response.length();
                            
                            
                        	user = new String[jumlah_data];
                        	gambar = new String[jumlah_data];
                        	fullname = new String[jumlah_data];
                        	id_user = new String[jumlah_data];
                        	
                        	for (int i = 0; i < response.length(); i++) {
                        	
                               JSONObject hasil = (JSONObject) response
                                        .get(i);
                                
                                	user[i] = hasil.getString("username");
                                    gambar[i] = hasil.getString("avatar");
                                    fullname[i] = hasil.getString("fullname");
                                    id_user[i] = hasil.getString("id_user");
                                
                        	}
                        } catch (JSONException e) {
                            /*e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                           
                        }
                        
                        Adapter_Pencarian_User adapter = new Adapter_Pencarian_User(Pelanggan.this, user, gambar, fullname);
                		hasil.setAdapter(adapter);
                        
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getActivity(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
                    	
                    	peringatan();
                    	
                        
                    }
                });
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(req);
	}

	@Override
	public void onItemClick(AdapterView arg0, View arg1, int posisi, long arg3) {

		Intent intent = new Intent(Pelanggan.this,Profile.class);
		intent.putExtra("datakirim", ""+id_user[posisi]);
		startActivity(intent);
		
		
	}
	

}
