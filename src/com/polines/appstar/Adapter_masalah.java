package com.polines.appstar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class Adapter_masalah extends ArrayAdapter<String> {

	String[] user={};
	String[] gambar={};
	String[] komentar={};
	String[] foto={};
	Context c;
	LayoutInflater inflater;
	
	
	public Adapter_masalah(Context context, String[]user, String[]gambar, String[]komentar, String[]foto) {
		super(context, R.layout.model_komentar,user);
		
		this.c = context;
		this.user = user;
		this.gambar = gambar;
		this.komentar = komentar;
		this.foto = foto;
	}
	
	public class ViewHolder
	{
		TextView user ;
		ImageView gambar,foto;
		TextView komentar;
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if(convertView == null)
		{
			inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.model_komentar,null);
		}
		
		final ViewHolder holder = new ViewHolder();
		holder.user = (TextView) convertView.findViewById(R.id.komentar_user);
		holder.gambar = (ImageView) convertView.findViewById(R.id.komentar_gambar);
		holder.user.setText(user[position]);
		holder.komentar = (TextView) convertView.findViewById(R.id.komentar_komentar);
		holder.komentar.setText(komentar[position]);
		
		
		
		Picasso.with(getContext())
		.load("http://appstar.esy.es/uploads/avatar/"+gambar[position])
		//.load("http://192.168.43.8/appstar/uploads/avatar/"+gambar[position])
		.resize(300, 300)
        .centerCrop()
        .transform(new CircleTransform())
        .into(holder.gambar);
		
		holder.foto = (ImageView) convertView.findViewById(R.id.komentar_foto);
		String cek = "";
		if(foto[position].equals(cek))
		{
			LinearLayout.LayoutParams GambarParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			GambarParams.height=0;
			holder.foto.setLayoutParams(GambarParams);
		}
		else
		{
			Picasso.with(getContext())
			.load("http://appstar.esy.es/uploads/komentar/"+foto[position])
			//.load("http://192.168.43.8/appstar/uploads/komentar/"+gambar[position])
			.resize(220, 120)
	        .centerCrop()
	        .into(holder.foto);
		}
		
		return convertView;
	}
	
	

}
